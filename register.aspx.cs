﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var test = 1;
        SqlDataReader dr;
        dr = DB.DbFunctions.GetWebSite();

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                litDisclaimer1.Text = Convert.ToString(dr["Disclaimer1"]);
                litDisclaimer2.Text = Convert.ToString(dr["Disclaimer2"]);
            }
        }

  
        dr.Dispose();
    }
}