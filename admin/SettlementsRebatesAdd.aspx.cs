﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SettlementsRebatesAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Settlements & Rebate";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            int id;
            if (!int.TryParse(hidID.Value, out id))
                id = int.MinValue;
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetSettlementsRebates(id);

        if (dr.HasRows)
        {
            dr.Read();
            imgThumbnail.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagessettlementsrebatesurl"] + Functions.ConvertToString(dr["Thumbnail"]);
            txtTitle.Text = Functions.ConvertToString(dr["Title"]);
            txtText.Text = Functions.ConvertToString(dr["Text"]);
            txtName.Text = Functions.ConvertToString(dr["Name"]);
            txtLinkURL.Text = Functions.ConvertToString(dr["Url"]);
            txtDayExpiry.Text = Functions.ConvertToString(dr["DateExpire"]);
            txtSettlementDeadline.Text = Functions.ConvertToString(dr["SettlementDeadline"]);
            chkActiveAll.Checked = Convert.ToBoolean(dr["IsActiveAll"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            txtEligible.Text = Functions.ConvertToString(dr["Eligible"]);
            txtPotentialAward.Text = Functions.ConvertToString(dr["PotentialAward"]);
            txtProofPurchase.Text = Functions.ConvertToString(dr["ProofPurchase"]);
            txtClaimFormDeadline.Text = Functions.ConvertToString(dr["ClaimFormDeadline"]);

            btnSave.Text = "Update " + ITEMNAME;
            lnkPreview.HRef = System.Configuration.ConfigurationManager.AppSettings["basepreviewurl"]+"Rebates/";
            lnkPreview.Visible = true;

            lnkSpecific.HRef = System.Configuration.ConfigurationManager.AppSettings["basepreviewurl"]+"Rebates/" + id + "/" + Functions.StrToURL(dr["Title"].ToString());
            lnkSpecific.Visible = true;

            lblActive.Text = "Active on " + System.Configuration.ConfigurationManager.AppSettings["Title"] + ": ";

            if (dr.NextResult())
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder sbWebSiteIDs = new StringBuilder();
                while (dr.Read())
                {
                    if (sbWebSiteIDs.Length > 0)
                        sbWebSiteIDs.Append(",");
                    sbWebSiteIDs.Append(Convert.ToString(dr["WebSiteID"]));

                    string fieldname = "ckb" + Convert.ToString(dr["WebSiteID"]);
                    string websitename = Convert.ToString(dr["WebSiteName"]);
                    string value;
                    if (Convert.ToBoolean(dr["IsActive"]))
                        value = "checked=\"checked\" ";
                    else
                        value = string.Empty;

                    sb.Append("<tr><td>&nbsp;</td><td><hr /></td></tr>");
                    sb.AppendFormat("<tr><td><label for=\"{0}\" id=\"lblfor{0}\">Active on {1}: </label></td>", fieldname, websitename);
                    sb.AppendFormat("<td><input id=\"{0}\" name=\"{0}\" type=\"checkbox\" {1}/></td></tr>", fieldname, value);
                }
                sb.Append("<tr><td>&nbsp;</td><td><hr /></td></tr>");
                sb.AppendFormat("<input type=\"hidden\" id=\"hidMoreWebSiteList\" name=\"hidMoreWebSiteList\" value=\"{0}\" />", sbWebSiteIDs.ToString());
                litMoreWebSite.Text = sb.ToString();
            }
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            lnkThumbnail.Disabled = true;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string thumbnail = String.Empty;
            if (fileThumbnail.HasFile)
            {
                thumbnail = Functions.RemoveSpecialChars(fileThumbnail.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("theclassactionguide", "images/settlementsRebates", ref thumbnail, fileThumbnail.PostedFile.InputStream);
            }

            int id = 0;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertSettlementsRebates(txtTitle.Text, txtLinkURL.Text, txtDayExpiry.Text, txtSettlementDeadline.Text, chkActiveAll.Checked, chkActive.Checked, txtText.Text, thumbnail, txtName.Text, txtEligible.Text, txtPotentialAward.Text, txtProofPurchase.Text, txtClaimFormDeadline.Text);

                //POLL INSERTED SUCCESSFULLY
                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                    ShowExistingValues();
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateSettlementsRebates(id, txtTitle.Text, txtLinkURL.Text, txtDayExpiry.Text, txtSettlementDeadline.Text, chkActiveAll.Checked, chkActive.Checked, txtText.Text, thumbnail, txtName.Text, txtEligible.Text, txtPotentialAward.Text, txtProofPurchase.Text, txtClaimFormDeadline.Text);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
            if (!String.IsNullOrEmpty(Request.Form["hidMoreWebSiteList"]))
            {
                string[] websites = Convert.ToString(Request.Form["hidMoreWebSiteList"]).Split(',');
                foreach (string websiteid in websites)
                {
                    DB.DbFunctions.UpdateSettlementsRebates(id, Convert.ToInt32(websiteid), Request.Form["ckb" + websiteid] != null && Request.Form["ckb" + websiteid] == "on", Request.Form["day" + websiteid]);
                }
            }
        }
    }
}