﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LifestyleCategoryAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Luck Letter Category";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            FillDdl();
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            int recipeid;
            if (!int.TryParse(hidID.Value, out recipeid))
                recipeid = int.MinValue;
        }
    }

    private void FillDdl()
    {
        SqlDataReader dr = DB.DbFunctions.GetLifestyleWebSites();
        ddlWebSite.DataSource = dr;
        ddlWebSite.DataTextField = "Description";
        ddlWebSite.DataValueField = "ID";
        ddlWebSite.DataBind();
        dr.Close();
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetOneLifestyleCategory(id);

        if (dr.HasRows)
        {
            dr.Read();

            txtTitle.Text = Functions.ConvertToString(dr["Description"]);
            ddlWebSite.SelectedValue = Functions.ConvertToString(dr["WebSiteID"]);
            btnSave.Text = "Update " + ITEMNAME;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertLifestyleCategory(txtTitle.Text, Convert.ToInt32(ddlWebSite.SelectedValue));

                //POLL INSERTED SUCCESSFULLY
                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateLifestyleCategory(id, txtTitle.Text, Convert.ToInt32(ddlWebSite.SelectedValue));

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
        }
    }

    private int ConvertToInt32(string str)
    {
        int i;
        if (!int.TryParse(str, out i))
        {
            i = int.MinValue;
        }
        return i;
    }
}