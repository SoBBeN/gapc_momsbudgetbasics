﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InternalAds.aspx.cs" Inherits="InternalAds" enableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
  <style>
    #right {
        float:right;
        width: 980px;
    }
  </style>
    <script type="text/javascript" src="scripts/jquery.uitablefilter.js"></script>
    <script type="text/javascript">
        $(function () {
            var theTable = $('#<%=gv.ClientID %>')

            theTable.find("tbody > tr").find("td:eq(1)").mousedown(function () {
                $(this).prev().find(":checkbox").click()
            });

            $("#filter").keyup(function () {
                $.uiTableFilter(theTable, this.value);
            })

            $('#filter-form').submit(function () {
                theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
                return false;
            }).focus(); //Give focus to input field

            $("#clearFilter").click(function () {
                document.getElementById("filter").value = "";
                $.uiTableFilter(theTable, "");
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="divFilter">
        <form id="filter-form">
            Filter: &nbsp; 
            <input type="text" name="filter" id="filter" value="" maxlength="30" size="30" />
            <input type="button" id="clearFilter" value="Clear" />
        </form>
    </div>
    <h3><%=ITEMNAME %></h3>
        <br />
        <a href="InternalAdsAdd.aspx">Add New <%=ITEMNAME %></a>
    <br />
    <div id="right">
        <div runat="server" id="divMsg" class="mInfo" visible="false">
        </div>
        <asp:GridView ID="gv" runat="server" Width="100%" GridLines="None" DataKeyNames="ID"
            OnRowDeleting="GvRowDeleting" OnRowCommand="GvRowCommand" CssClass="grid">
            <HeaderStyle CssClass="gridHead" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="InternalAdsAdd.aspx?id={0}"
                    Text="Edit" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Delete"
                            Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this ad?')" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CommandName="MoveUp"
                            Text="Move Up" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" CommandName="MoveDown"
                            Text="Move Down" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div style="clear:both;"></div>
</asp:Content>
