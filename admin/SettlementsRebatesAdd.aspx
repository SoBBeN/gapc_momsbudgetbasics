﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SettlementsRebatesAdd.aspx.cs" Inherits="SettlementsRebatesAdd" ValidateRequest="false" %>
<%@ Register Assembly="MyClassLibrary" Namespace="MyClassLibrary.Controls" TagPrefix="CustomControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script src="ckeditor446/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDayExpiry.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
            $("#<%=txtSettlementDeadline.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
            $("#<%=txtClaimFormDeadline.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            }); 
        });
    </script>
	<style>
	  .selectable .ui-selecting { background: #FECA40; }
	  .selectable .ui-selected { background: #F39814; color: white; }
	  .selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	  .selectable li { margin: 3px; padding: 1px; float: left; width: 183px; height: 25px; font-size: 18px; font-weight:bold; line-height:25px; text-align: center; }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <div>
            <img runat="server" id="imgThumbnail" style="max-height:225px; max-width:350px;" />
        </div><br />
        <table width="100%">
            
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblThumbnail" AssociatedControlID="fileThumbnail" Text="Thumbnail File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fileThumbnail" /> <a runat="server" id="lnkThumbnail">Edit from Image</a>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblName" AssociatedControlID="txtTitle" Text="Name:" />
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblLinkURL" AssociatedControlID="txtLinkURL" Text="Link:" />
                </td>
                <td>
                    <asp:TextBox ID="txtLinkURL" runat="server" CssClass="text" Columns="150" MaxLength="150" Width="250px" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtLinkURL" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescription" AssociatedControlID="txtText" Text="Text:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtText" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtText.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblEligible" AssociatedControlID="txtEligible" Text="Who’s Eligible:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtEligible" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtEligible.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblPotentialAward" AssociatedControlID="txtPotentialAward" Text="Potential Award:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtPotentialAward" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtPotentialAward.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblProofPurchase" AssociatedControlID="txtProofPurchase" Text="Proof of Purchase:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtProofPurchase" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtProofPurchase.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblClaimFormDeadline" AssociatedControlID="txtClaimFormDeadline" Text="Claim Form Deadline:" />
                </td>
                <td>
                    <asp:TextBox ID="txtClaimFormDeadline" runat="server" CssClass="text" Columns="10" MaxLength="10" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActiveAll" AssociatedControlID="chkActiveAll" Text="Main Active (All Websites):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActiveAll" Checked="false" /> (When checked, offer is live on all websites below)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDayExpiry" AssociatedControlID="txtDayExpiry" Text="Expiry Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDayExpiry" runat="server" CssClass="text" Columns="10" MaxLength="10" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblSettlementDeadline" AssociatedControlID="txtSettlementDeadline" Text="Settlement Deadline:" />
                </td>
                <td>
                    <asp:TextBox ID="txtSettlementDeadline" runat="server" CssClass="text" Columns="10" MaxLength="10" />&nbsp;
                </td>
            </tr>
            <tr><td>&nbsp;</td><td><hr /></td></tr>
            <tr>
                <td style="white-space:nowrap">
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status (This Website):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" />
                </td>
            </tr>
            <asp:Literal runat="server" ID="litMoreWebSite" />
            
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
        <a target="_blank" runat="server" id="lnkSpecific" visible="false"> See Specific Page</a>
    </div>
</asp:Content>

