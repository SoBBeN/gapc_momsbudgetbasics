﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LifestyleTagAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Luck Letter Tag";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            int recipeid;
            if (!int.TryParse(hidID.Value, out recipeid))
                recipeid = int.MinValue;
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetOneLifestyleTag(id);

        if (dr.HasRows)
        {
            dr.Read();

            txtTitle.Text = Functions.ConvertToString(dr["Description"]);
            btnSave.Text = "Update " + ITEMNAME;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertLifestyleTag(txtTitle.Text);

                //POLL INSERTED SUCCESSFULLY
                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateLifestyleTag(id, txtTitle.Text);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
        }
    }

    private int ConvertToInt32(string str)
    {
        int i;
        if (!int.TryParse(str, out i))
        {
            i = int.MinValue;
        }
        return i;
    }
}