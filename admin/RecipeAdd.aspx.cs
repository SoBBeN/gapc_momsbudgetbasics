﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RecipeAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Recipe";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            int recipeid;
            if (!int.TryParse(hidID.Value, out recipeid))
                recipeid = int.MinValue;

            FillDdl(DB.DbFunctions.GetRecipeCuisine(recipeid), sltCuisine);
            FillDdl(DB.DbFunctions.GetRecipeMeal(recipeid), sltMeal);
            FillDdl(DB.DbFunctions.GetRecipeCategory(recipeid), sltCategory);
        }
    }

    private void FillDdl(SqlDataReader dr, MyClassLibrary.Controls.Selectable slt)
    {
        if (dr != null)
        {
            if (dr.HasRows)
            {
                /*slt.DataSource = dr;
                slt.DataTextField = "Description";
                slt.DataValueField = "ID";
                slt.DataBind();*/
                while (dr.Read())
                {
                    ListItem i = new ListItem(Convert.ToString(dr["Description"]), Convert.ToString(dr["ID"]));
                    i.Selected = Convert.ToBoolean(dr["IsSelected"]);
                    slt.Items.Add(i);
                }
            }
            dr.Close();
     dr.Dispose();
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetRecipe(id);

        if (dr.HasRows)
        {
            dr.Read();

            txtTimeCook.Text = Functions.ConvertToString(dr["CookTime"]);
            txtTimePrep.Text = Functions.ConvertToString(dr["PrepTime"]);
            txtTimeTotal.Text = Functions.ConvertToString(dr["TotalTime"]);
            txtPortions.Text = Functions.ConvertToString(dr["Portions"]);
            txtAuthor.Text = Functions.ConvertToString(dr["Author"]);
            txtAuthorLink.Text = Functions.ConvertToString(dr["AuthorLink"]);
            txtTitle.Text = Functions.ConvertToString(dr["Title"]);
            txtDescription.Text = Functions.ConvertToString(dr["Description"]);
            txtIngredients.Text = Functions.ConvertToString(dr["Ingredients"]);
            txtMethod.Text = Functions.ConvertToString(dr["Method"]);
            txtDay.Text = Functions.ConvertToString(dr["ActiveDate"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            chkActiveAll.Checked = Convert.ToBoolean(dr["IsActiveAll"]);
            lnkPreview.HRef = "http://dev.mommysfreebies.com.com/recipe/" + id + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/";
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "recipe/" + Functions.ConvertToString(dr["ImageFilename"]);
            lnkPreview.Visible = true;
            RequiredFieldValidator0.Enabled = false;
            btnSave.Text = "Update " + ITEMNAME;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
     dr.Dispose();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;

            string filename = String.Empty;
            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);
                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/recipe", ref filename, ImageFile.PostedFile.InputStream);
            }

            string authorImage = String.Empty;
            if (fileAuthorImage.HasFile)
            {
                authorImage = Functions.RemoveSpecialChars(fileAuthorImage.FileName);
                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/recipe", ref authorImage, fileAuthorImage.PostedFile.InputStream);
            }

            string activeDate;
            if (txtDay.Text.Length > 7)
                activeDate = txtDay.Text;
            else
                activeDate = null;

            if (hidID.Value.Length == 0) //INSERT
            {
                if (filename.Length > 0)
                {
                    id = DB.DbFunctions.InsertRecipe(filename, ConvertToInt32(txtTimeCook.Text), ConvertToInt32(txtTimePrep.Text), ConvertToInt32(txtTimeTotal.Text), ConvertToInt32(txtPortions.Text), txtAuthor.Text, txtTitle.Text, txtDescription.Text, txtIngredients.Text, txtMethod.Text, chkActiveAll.Checked, chkActive.Checked, activeDate, txtAuthorLink.Text, authorImage);

                    //POLL INSERTED SUCCESSFULLY
                    if (id > 0)
                    {
                        hidID.Value = id.ToString();
                        divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                        divMsg.Visible = true;
                    }
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateRecipe(id, filename, ConvertToInt32(txtTimeCook.Text), ConvertToInt32(txtTimePrep.Text), ConvertToInt32(txtTimeTotal.Text), ConvertToInt32(txtPortions.Text), txtAuthor.Text, txtTitle.Text, txtDescription.Text, txtIngredients.Text, txtMethod.Text, chkActiveAll.Checked, chkActive.Checked, activeDate, txtAuthorLink.Text, authorImage);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
            if (id > 0)
            {
                foreach (ListItem item in sltMeal.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertRecipeMeal(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategory.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertRecipeCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCuisine.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertRecipeCuisine(id, Convert.ToInt32(item.Value));
                }
                ShowExistingValues();
            }
        }
    }

    private int ConvertToInt32(string str)
    {
        int i;
        if (!int.TryParse(str, out i))
        {
            i = int.MinValue;
        }
        return i;
    }
}