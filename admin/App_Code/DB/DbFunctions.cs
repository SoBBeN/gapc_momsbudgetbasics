﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

namespace DB
{

    public class DbFunctions
    {
        private static int WebSiteID
        {
            get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["websiteid"]); }
        }

        public static DataRow Authenticate(string username, string password)
        {
            StringBuilder query = new StringBuilder("[BF_Admins_Authenticate] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(username)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password)));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            //HttpContext.Current.Response.Write(query.ToString());
            return objSqlServer.GetFirstRow(query.ToString());
        }




        public static SqlDataReader GetAllNews()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[News_Get] ").Append("1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetNews(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[News_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteNews(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[News_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateNews(int id, string imageFilename, string webSiteName, string webSiteLink, string title, string shorttext, string text,
                                         bool isActive, string activeDate, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[News_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteName)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteLink)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(shorttext)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertNews(string imageFilename, string webSiteName, string webSiteLink, string title, string shorttext, string text,
                                         bool isActive, string activeDate, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[News_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteName)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteLink)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(shorttext)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }






        public static SqlDataReader GetProductCategory(int Productid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Category_GetAll] ");
            query.Append(Productid);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllProduct()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Get] ").Append("1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetProduct(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_GetOne] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteProduct(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateProduct(int id, string imageFilename, string webSiteName, string webSiteLink, string title, string description,
                                         bool isActiveAll, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Product_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteName)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteLink)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateProduct(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Product_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            HttpContext.Current.Response.Write(query.ToString());
            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertProduct(string imageFilename, string webSiteName, string webSiteLink, string title, string description,
                                         bool isActiveAll, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Product_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteName)).Append(",");
            query.Append(objSqlServer.FormatSql(webSiteLink)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void InsertProductCategory(int Productid, int category)
        {
            StringBuilder query = new StringBuilder("[ProductToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(Productid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateProductThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[Product_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }


        public static SqlDataReader GetAllProductCategory()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[ProductCategory_GetAll] 1");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetProductCategoryGrid()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[ProductCategory_GetAll] 0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetOneProductCategory(int Productid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[ProductCategory_GetOne] ");
            query.Append(Productid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteProductCategory(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[ProductCategory_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateProductCategory(int id, string imageFilename, string description)
        {
            StringBuilder query = new StringBuilder("[ProductCategory_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(description));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertProductCategory(string imageFilename, string description)
        {
            StringBuilder query = new StringBuilder("[ProductCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(description));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void UpdateOrderProductCategory(string[] strOffers)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query;
            Boolean first = true;

            for (int i = 0; i < strOffers.Length; i++)
            {
                string[] strValues = strOffers[i].Split(':');
                if (strValues.Length == 2)
                {
                    query = new StringBuilder("[ProductCategory_UpdateOrder] ").Append(strValues[0]).Append(",").Append(strValues[1]).Append(",").Append(first);

                    query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
                    objSqlServer.ExecNonQuery(query.ToString());
                    first = false;
                }
            }
        }








        public static SqlDataReader GetAllLifestyle()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Get] ").Append("1,0");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllLifestyleSlide()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_Get] ").Append("1,0");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllLifestyleSlide(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_Get] ").Append("1,0," + lifestyleid.ToString());

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleSlide(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteLifestyle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteLifestyleSlide(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyle(int id, string title,
                                         bool isActive, string activeDate, string subtitle)
        {
            StringBuilder query = new StringBuilder("[Lifestyle_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleSlide(int id, int lifestyleid, string imageFilename, string subtitle, string description,
                                         bool isActive, string activeDate, string thumbnail, string descriptiontop, bool imageVisible, string VideoScript)
        {
            StringBuilder query = new StringBuilder("[LifestyleSlide_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(descriptiontop)).Append(",");
            query.Append(objSqlServer.FormatSql(imageVisible)).Append(",");
            query.Append(objSqlServer.FormatSql(VideoScript));


            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertLifestyle(string title,
                                         bool isActive, string activeDate, string subtitle)
        {
            StringBuilder query = new StringBuilder("[Lifestyle_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static int InsertLifestyleSlide(string imageFilename, int lifestyleid, string subtitle, string description,
                                         bool isActive, string activeDate, string thumbnail, string descriptiontop, bool imageVisible, string VideoScript)
        {
            StringBuilder query = new StringBuilder("[LifestyleSlide_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(descriptiontop)).Append(",");
            query.Append(objSqlServer.FormatSql(imageVisible)).Append(",");
            query.Append(objSqlServer.FormatSql(VideoScript));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void MoveLifestyleSlide(int slideid, bool moveup)
        {
            StringBuilder query = new StringBuilder("[LifestyleSlide_Move] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(slideid)).Append(",");
            query.Append(objSqlServer.FormatSql(moveup));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetLifestyleWebSites()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetWebsites] ");
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleCategory(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Category_GetAll] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader LifestyleGetByCategory(int CategoryID)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Get_ByCategory_No_Website] ");
            query.Append(CategoryID);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleCategoryDistinct()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Category_GetAll_Distinct] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void InsertLifestyleCategory(int lifestyleid, int category)
        {
            StringBuilder query = new StringBuilder("[LifestyleToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[Lifestyle_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }

        public static SqlDataReader GetLifestyleCategory()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategory_GetAll]");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetOneLifestyleCategory(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategory_GetOne] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteLifestyleCategory(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategory_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleCategory(int id, string description, int websiteid)
        {
            StringBuilder query = new StringBuilder("[LifestyleCategory_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertLifestyleCategory(string description, int websiteid)
        {
            StringBuilder query = new StringBuilder("[LifestyleCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(description));
            query.Append(",").Append(objSqlServer.FormatSql(websiteid));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }



        public static SqlDataReader GetLifestyleTag(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Tag_GetAll] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void InsertLifestyleTag(int lifestyleid, int tag)
        {
            StringBuilder query = new StringBuilder("[LifestyleToTag_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(tag));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetLifestyleTag()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_GetAll]");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetOneLifestyleTag(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_GetOne] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteLifestyleTag(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleTag(int id, string description)
        {
            StringBuilder query = new StringBuilder("[LifestyleTag_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(description));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertLifestyleTag(string description)
        {
            StringBuilder query = new StringBuilder("[LifestyleTag_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(description));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }








        

        public static SqlDataReader GetRecipeCuisine(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Cuisine_GetAll] ");
            query.Append(recipeid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRecipeMeal(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Meal_GetAll] ");
            query.Append(recipeid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRecipeCategory(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Category_GetAll] ");
            query.Append(recipeid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllRecipe()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Get] ").Append("1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRecipe(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_GetOne] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteRecipe(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateRecipe(int id, string imageFilename, int cooktime, int preptime, int totaltime, int portions, string author, string title, string description, string ingredients,
                                         string method, bool isActiveAll, bool isActive, string activeDate, string authorlink, string authorimage)
        {
            StringBuilder query = new StringBuilder("[Recipe_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(cooktime)).Append(",");
            query.Append(objSqlServer.FormatSql(preptime)).Append(",");
            query.Append(objSqlServer.FormatSql(totaltime)).Append(",");
            query.Append(objSqlServer.FormatSql(portions)).Append(",");
            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(ingredients)).Append(",");
            query.Append(objSqlServer.FormatSql(method)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(authorlink)).Append(",");
            query.Append(objSqlServer.FormatSql(authorimage));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateRecipe(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Recipe_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            HttpContext.Current.Response.Write(query.ToString());
            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertRecipe(string imageFilename, int cooktime, int preptime, int totaltime, int portions, string author, string title, string description, string ingredients,
                                         string method, bool isActiveAll, bool isActive, string activeDate, string authorlink, string authorimage)
        {
            StringBuilder query = new StringBuilder("[Recipe_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(cooktime)).Append(",");
            query.Append(objSqlServer.FormatSql(preptime)).Append(",");
            query.Append(objSqlServer.FormatSql(totaltime)).Append(",");
            query.Append(objSqlServer.FormatSql(portions)).Append(",");
            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(ingredients)).Append(",");
            query.Append(objSqlServer.FormatSql(method)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(authorlink)).Append(",");
            query.Append(objSqlServer.FormatSql(authorimage));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void InsertRecipeCategory(int recipeid, int category)
        {
            StringBuilder query = new StringBuilder("[RecipeToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void InsertRecipeCuisine(int recipeid, int cuisine)
        {
            StringBuilder query = new StringBuilder("[RecipeToCuisine_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(cuisine));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void InsertRecipeMeal(int recipeid, int meal)
        {
            StringBuilder query = new StringBuilder("[RecipeToMeal_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(meal));

            objSqlServer.ExecNonQuery(query.ToString());
        }





        public static SqlDataReader GetEditorPick(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[EditorPicks_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetEditorPick()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[EditorPicks_Get] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllEditorPick()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[EditorPicks_Get] 1");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateOrderEditorPick(string[] strOffers)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query;
            Boolean first = true;

            for (int i = 0; i < strOffers.Length; i++)
            {
                string[] strValues = strOffers[i].Split(':');
                if (strValues.Length == 2)
                {
                    query = new StringBuilder("[EditorPicks_UpdateOrder] ").Append(strValues[0]).Append(",").Append(strValues[1]).Append(",").Append(first);
                    objSqlServer.ExecNonQuery(query.ToString());
                    first = false;
                }
            }
        }
        public static void DeleteEditorPick(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[EditorPicks_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateEditorPick(int id, string title, string shortdesc, string link)
        {
            StringBuilder query = new StringBuilder("[EditorPicks_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(shortdesc)).Append(",");
            query.Append(objSqlServer.FormatSql(link));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertEditorPick(string title, string shortdesc, string link)
        {
            StringBuilder query = new StringBuilder("[EditorPicks_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(shortdesc)).Append(",");
            query.Append(objSqlServer.FormatSql(link));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }




        public static SqlDataReader GetSlider(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Slider_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSlider()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Slider_Get] 0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllSliders()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Slider_Get] 1");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateOrderSlider(string[] strOffers)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query;
            Boolean first = true;

            for (int i = 0; i < strOffers.Length; i++)
            {
                string[] strValues = strOffers[i].Split(':');
                if (strValues.Length == 2)
                {
                    query = new StringBuilder("[BF_Slider_UpdateOrder] ").Append(strValues[0]).Append(",").Append(strValues[1]).Append(",").Append(first);

                    query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
                    objSqlServer.ExecNonQuery(query.ToString());
                    first = false;
                }
            }
        }
        public static void DeleteSlider(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Slider_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSlider(int id, string imageFilename, string linkURL, short effect, string title, bool newwindow)
        {
            StringBuilder query = new StringBuilder("[BF_Slider_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(effect)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(newwindow));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertSlider(string imageFilename, string linkURL, short effect, string title, bool newwindow)
        {
            StringBuilder query = new StringBuilder("[BF_Slider_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(effect)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(newwindow));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static SqlDataReader GetSliderEffects()
        {
            StringBuilder query = new StringBuilder("[Slider_Effects_Get] ");
            SqlServer objSqlServer = new SqlServer();

            return objSqlServer.GetDataReader(query.ToString());
        }





        public static SqlDataReader GetPointsOffers(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_GetOne] ").Append(id);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllPointsOffers()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_Get] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPointsOffersCategory(int pointsOffersid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_Category_GetAll] ");
            query.Append(pointsOffersid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeletePointsOffers(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdatePointsOffers(int id, string title, string url, string nbPoints, string dateActive, string dateExpire, bool isActiveAll, bool isActive, string text, string imageFilename, string thumbnail, string name)
        {
            StringBuilder query = new StringBuilder("[PointsOffers_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(nbPoints)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(dateActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdatePointsOffersThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[PointsOffers_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdatePointsOffers(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[PointsOffers_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            //HttpContext.Current.Response.Write(query.ToString());
            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertPointsOffers(string title, string url, string nbPoints, string dateActive, string dateExpire, bool isActiveAll, bool isActive, string text, string imageFilename, string thumbnail, string name)
        {
            StringBuilder query = new StringBuilder("[PointsOffers_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(nbPoints)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(dateActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void InsertPointsOffersCategory(int pointsOffersid, int category)
        {
            StringBuilder query = new StringBuilder("[PointsOffersToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(pointsOffersid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void PointsOffersInsertCategory(string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_Insert_Category] ").Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }



        public static SqlDataReader GetCarousel(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Carousel_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetCarousel()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Carousel_Get] 0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllCarousels()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Carousel_Get] 1");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateOrderCarousel(string[] strOffers)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query;
            Boolean first = true;

            for (int i = 0; i < strOffers.Length; i++)
            {
                string[] strValues = strOffers[i].Split(':');
                if (strValues.Length == 2)
                {
                    query = new StringBuilder("[BF_Carousel_UpdateOrder] ").Append(strValues[0]).Append(",").Append(strValues[1]).Append(",").Append(first);

                    query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
                    objSqlServer.ExecNonQuery(query.ToString());
                    first = false;
                }
            }
        }
        public static void DeleteCarousel(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Carousel_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateCarousel(int id, string imageFilename, string linkURL, string title)
        {
            StringBuilder query = new StringBuilder("[BF_Carousel_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(title));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertCarousel(string imageFilename, string linkURL, string title)
        {
            StringBuilder query = new StringBuilder("[BF_Carousel_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(title));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }





        public static SqlDataReader GetTalkingPoint()
        {
            return GetTalkingPoint(int.MinValue);
        }
        public static SqlDataReader GetTalkingPoint(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[TalkingPoints_Get] ").Append(objSqlServer.FormatSql(id)).Append(",1");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteTalkingPoint(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[TalkingPoints_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateTalkingPoint(int id, string question, bool active)
        {
            StringBuilder query = new StringBuilder("[TalkingPoints_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(question)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertTalkingPoint(string question, bool active)
        {
            StringBuilder query = new StringBuilder("[TalkingPoints_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(question)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }

        public static SqlDataReader GetArticle()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Get] ").Append("NULL,0,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetArticle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Get] ").Append(objSqlServer.FormatSql(id)).Append(",1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteArticle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateArticle(int id, string author, string title, string titleshort, string text, string category, bool inMenu, bool active, string imageFilename)
        {
            StringBuilder query = new StringBuilder("[Articles_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(titleshort)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(inMenu)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertArticle(string author, string title, string titleshort, string text, string category, bool inMenu, bool active, string imageFilename)
        {
            StringBuilder query = new StringBuilder("[Articles_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(titleshort)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(inMenu)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }




        public static SqlDataReader GetAllContent()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_Get] ").Append("1,0");

            query.Append(',').Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetContent(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_GetOne] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void MoveContent(int id, bool moveup)
        {
            StringBuilder query = new StringBuilder("[Content_Move] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(moveup));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteContent(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteContentImage(int id)
        {
            StringBuilder query = new StringBuilder("[Content_DeleteImage] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateContent(int id, string title, string category, string desctop, string descbtm, bool active, string imageFilename, string imageLink)
        {
            StringBuilder query = new StringBuilder("[Content_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(desctop)).Append(",");
            query.Append(objSqlServer.FormatSql(descbtm)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(',');
            query.Append(objSqlServer.FormatSql(imageLink));


            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertContent(string title, string category, string desctop, string descbtm, bool active, string imageFilename, string imageLink)
        {
            StringBuilder query = new StringBuilder("[Content_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(desctop)).Append(",");
            query.Append(objSqlServer.FormatSql(descbtm)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(',');
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(',');
            query.Append(objSqlServer.FormatSql(imageLink));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }






        public static SqlDataReader GetAdmins()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Admins_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAdmin(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Admins_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteAdmin(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Admins_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateAdmin(int id, string username, string password, bool active, bool editusers)
        {
            StringBuilder query = new StringBuilder("[BF_Admins_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(username)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(editusers));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertAdmin(string username, string password, bool active, bool editusers)
        {
            StringBuilder query = new StringBuilder("[BF_Admins_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(username)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(editusers));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }







        public static SqlDataReader GetTwitters()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Twitter_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTwitter(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Twitter_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteTwitter(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Twitter_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateTwitter(int id, string title, string code, bool active)
        {
            StringBuilder query = new StringBuilder("[Twitter_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(code)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertTwitter(string title, string code, bool active)
        {
            StringBuilder query = new StringBuilder("[Twitter_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(code)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }







        public static SqlDataReader GetUsers()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_User_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetUser(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_User_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteUser(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_User_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateUser(int id, string firstname, string lastname, string email, string zip, string password, bool active)
        {
            StringBuilder query = new StringBuilder("[BF_User_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            if (password.Length > 0)
                query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            else
                query.Append("NULL,");
            query.Append(objSqlServer.FormatSql(active));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertUser(string firstname, string lastname, string email, string zip, string password, bool active)
        {
            StringBuilder query = new StringBuilder("[BF_User_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }




        public static SqlDataReader GetScrubs()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Scrub_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetScrub(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Scrub_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteScrub(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Scrub_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateScrub(int id, string title, short scrub, bool active)
        {
            StringBuilder query = new StringBuilder("[Scrub_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(scrub)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertScrub(string title, short scrub, bool active)
        {
            StringBuilder query = new StringBuilder("[Scrub_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(scrub)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static SqlDataReader GetAffiliates()
        {
            StringBuilder query = new StringBuilder("[Affiliate_Get] ");
            SqlServer objSqlServer = new SqlServer();


            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetScrubAffiliates(int scrubid)
        {
            StringBuilder query = new StringBuilder("[Scrub_Affiliate_Get] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(scrubid));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void InsertAffScrub(int id, int affid, short scrub)
        {
            StringBuilder query = new StringBuilder("[Scrub_Affiliate_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(affid)).Append(",");
            query.Append(objSqlServer.FormatSql(scrub));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteAffScrub(int id, int affid)
        {
            StringBuilder query = new StringBuilder("[Scrub_Affiliate_Delete] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(affid));

            objSqlServer.ExecNonQuery(query.ToString());
        }





        public static SqlDataReader GetWebSites()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Website_GetAll] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetWebSite(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Website_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateWebSite(int id, string description, string disclaimer1, string disclaimer2, string privacy, string terms, string howItWorks, string PrivacyRequestForm, string PrivacyNotice, string AccessibilityNotice, string disclaimer3, string disclaimer4, string ContentHeader, string ContentHeaderMobile, string MenuText)
        {
            StringBuilder query = new StringBuilder("[Website_Update_Content] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer1)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer2)).Append(",");
            query.Append(objSqlServer.FormatSql(privacy)).Append(",");
            query.Append(objSqlServer.FormatSql(terms)).Append(",");
            query.Append(objSqlServer.FormatSql(howItWorks)).Append(",");
            query.Append(objSqlServer.FormatSql(PrivacyRequestForm)).Append(",");
            query.Append(objSqlServer.FormatSql(PrivacyNotice)).Append(",");
            query.Append(objSqlServer.FormatSql(AccessibilityNotice)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer3)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer4)).Append(",");
            query.Append(objSqlServer.FormatSql(ContentHeader)).Append(",");
            query.Append(objSqlServer.FormatSql(ContentHeaderMobile)).Append(",");
            query.Append(objSqlServer.FormatSql(MenuText));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertWebSite(string description, string disclaimer1, string disclaimer2, string privacy, string terms, string howItWorks, string PrivacyRequestForm, string PrivacyNotice, string AccessibilityNotice, string disclaimer3, string disclaimer4, string ContentHeader, string ContentHeaderMobile, string MenuText)
        {
            StringBuilder query = new StringBuilder("[Website_Insert_Content] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer1)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer2)).Append(",");
            query.Append(objSqlServer.FormatSql(privacy)).Append(",");
            query.Append(objSqlServer.FormatSql(terms)).Append(",");
            query.Append(objSqlServer.FormatSql(howItWorks)).Append(",");
            query.Append(objSqlServer.FormatSql(PrivacyRequestForm)).Append(",");
            query.Append(objSqlServer.FormatSql(PrivacyNotice)).Append(",");
            query.Append(objSqlServer.FormatSql(AccessibilityNotice)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer3)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer4)).Append(",");
            query.Append(objSqlServer.FormatSql(ContentHeader)).Append(",");
            query.Append(objSqlServer.FormatSql(ContentHeaderMobile)).Append(",");
            query.Append(objSqlServer.FormatSql(MenuText));


            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }



        public static SqlDataReader GetTodaysMom(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTodaysMom()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Get] 0,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllTodaysMom()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Get] 1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateOrderTodaysMom(string[] strOffers)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query;
            Boolean first = true;

            for (int i = 0; i < strOffers.Length; i++)
            {
                string[] strValues = strOffers[i].Split(':');
                if (strValues.Length == 2)
                {
                    query = new StringBuilder("[BF_TodaysMom_UpdateOrder] ").Append(strValues[0]).Append(",").Append(strValues[1]).Append(",").Append(first);

                    query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
                    objSqlServer.ExecNonQuery(query.ToString());
                    first = false;
                }
            }
        }
        public static void DeleteTodaysMom(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateTodaysMom(int id, string imageFilename, string name, string location, string occupation, string text, string linkURL
            , string linktitle, string linklogo, string facebook, string twitter, string pinterest, bool isActive, string instagram, string youtube, string activedate)
        {
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(location)).Append(",");
            query.Append(objSqlServer.FormatSql(occupation)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(linktitle)).Append(",");
            query.Append(objSqlServer.FormatSql(linklogo)).Append(",");
            query.Append(objSqlServer.FormatSql(facebook)).Append(",");
            query.Append(objSqlServer.FormatSql(twitter)).Append(",");
            query.Append(objSqlServer.FormatSql(pinterest)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(instagram)).Append(",");
            query.Append(objSqlServer.FormatSql(youtube)).Append(",");
            query.Append(objSqlServer.FormatSql(activedate));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertTodaysMom(string imageFilename, string name, string location, string occupation, string text, string linkURL
            , string linktitle, string linklogo, string facebook, string twitter, string pinterest, bool isActive, string instagram, string youtube, string activedate)
        {
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(location)).Append(",");
            query.Append(objSqlServer.FormatSql(occupation)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(linktitle)).Append(",");
            query.Append(objSqlServer.FormatSql(linklogo)).Append(",");
            query.Append(objSqlServer.FormatSql(facebook)).Append(",");
            query.Append(objSqlServer.FormatSql(twitter)).Append(",");
            query.Append(objSqlServer.FormatSql(pinterest)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(instagram)).Append(",");
            query.Append(objSqlServer.FormatSql(youtube)).Append(",");
            query.Append(objSqlServer.FormatSql(activedate));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static SqlDataReader GetTodaysMomImages(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Image_Get] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteTodaysMomImage(string momid, string imageid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Image_Delete] ").Append(momid).Append(",").Append(imageid);

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void SetDefaultTodaysMomImage(string momid, string imageid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Image_SetDefault] ").Append(momid).Append(",").Append(imageid);

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void AddTodaysMomImage(int momid, string imagefilename)
        {
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Image_Add] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(momid)).Append(",");
            query.Append(objSqlServer.FormatSql(imagefilename));

            objSqlServer.ExecNonQuery(query.ToString());
        }



        public static SqlDataReader GetFbDay(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetFbDay()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Get] 0,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllFbDay()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Get] 1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteFbDay(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateFbDay(int id, string imageFilename, string linkURL, string text, string day, string title, string fulltext, bool isActive)
        {
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(day)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(fulltext)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertFbDay(string imageFilename, string linkURL, string text, string day, string title, string fulltext, bool isActive)
        {
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(linkURL)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(day)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(fulltext)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }







        public static SqlDataReader GetBloggers(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetBloggers()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Get] 0,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllBloggers()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Get] 1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteBloggers(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateBloggers(int id, string imageFilename, string text, string nickname, string firstname, string lastname, bool isActive, string thumbnailfile)
        {
            StringBuilder query = new StringBuilder("[Bloggers_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(nickname)).Append(",");
            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive));
            query.Append(",").Append(objSqlServer.FormatSql(thumbnailfile));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertBloggers(string imageFilename, string text, string nickname, string firstname, string lastname, bool isActive, string thumbnailfile)
        {
            StringBuilder query = new StringBuilder("[Bloggers_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(nickname)).Append(",");
            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            query.Append(",").Append(objSqlServer.FormatSql(thumbnailfile));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void UpdateBloggerThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[Bloggers_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetBlogs(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_Get] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetBlogs()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_GetAll] 0,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllBlogs()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_GetAll] 1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateOrderBlog(string[] strOffers)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query;
            Boolean first = true;

            for (int i = 0; i < strOffers.Length; i++)
            {
                string[] strValues = strOffers[i].Split(':');
                if (strValues.Length == 2)
                {
                    query = new StringBuilder("[BF_Blog_UpdateOrder] ").Append(strValues[0]).Append(",").Append(strValues[1]).Append(",").Append(first);

                    query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
                    objSqlServer.ExecNonQuery(query.ToString());
                    first = false;
                }
            }
        }
        public static void DeleteBlogs(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateBlogs(int id, string imageFilename, int author, string text, string title, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Blogs_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertBlogs(string imageFilename, int author, string text, string title, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Blogs_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }




        public static SqlDataReader GetDaycare(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Daycare_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetDaycare()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Daycare_Get] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllDaycare()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Daycare_Get] 1");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteDaycare(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Daycare_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateDaycare(int id, string name, string address, string city, string state, string zip, bool baby, bool preschool, bool afterschool, decimal latitude, decimal longitude, string website, string phone, bool isactive)
        {
            StringBuilder query = new StringBuilder("[Daycare_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(address)).Append(",");
            query.Append(objSqlServer.FormatSql(city)).Append(",");
            query.Append(objSqlServer.FormatSql(state)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            query.Append(objSqlServer.FormatSql(baby)).Append(",");
            query.Append(objSqlServer.FormatSql(preschool)).Append(",");
            query.Append(objSqlServer.FormatSql(afterschool)).Append(",");
            query.Append(objSqlServer.FormatSql(latitude)).Append(",");
            query.Append(objSqlServer.FormatSql(longitude)).Append(",");
            query.Append(objSqlServer.FormatSql(website)).Append(",");
            query.Append(objSqlServer.FormatSql(phone)).Append(",");
            query.Append(objSqlServer.FormatSql(isactive));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertDaycare(string name, string address, string city, string state, string zip, bool baby, bool preschool, bool afterschool, decimal latitude, decimal longitude, string website, string phone, bool isactive)
        {
            StringBuilder query = new StringBuilder("[Daycare_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(address)).Append(",");
            query.Append(objSqlServer.FormatSql(city)).Append(",");
            query.Append(objSqlServer.FormatSql(state)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            query.Append(objSqlServer.FormatSql(baby)).Append(",");
            query.Append(objSqlServer.FormatSql(preschool)).Append(",");
            query.Append(objSqlServer.FormatSql(afterschool)).Append(",");
            query.Append(objSqlServer.FormatSql(latitude)).Append(",");
            query.Append(objSqlServer.FormatSql(longitude)).Append(",");
            query.Append(objSqlServer.FormatSql(website)).Append(",");
            query.Append(objSqlServer.FormatSql(phone)).Append(",");
            query.Append(objSqlServer.FormatSql(isactive));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }





        public static SqlDataReader GetImage(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetImage()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get] 0,NULL,NULL");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllImage(bool isMoments)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get] 1,null,").Append(objSqlServer.FormatSql(isMoments));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetImageToApprove()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_GetToApprove] ");
            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteImage(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateImage(int id, short type, string title, string imageFilename, int isActive, string text, string activeDate, string thumbnail, string overrideUsername)
        {
            StringBuilder query = new StringBuilder("[BF_Image_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(type)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(overrideUsername));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateImage(int id, short type, int isActive, string text, string overrideUsername)
        {
            StringBuilder query = new StringBuilder("[BF_Image_Update_ApprovePage] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(type)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(overrideUsername));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateImage(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[BF_Image_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertImage(short type, string title, string imageFilename, int isActive, string text, string activeDate, string thumbnail, string overrideUsername)
        {
            StringBuilder query = new StringBuilder("[BF_Image_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(type)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(overrideUsername));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static SqlDataReader GetImageTypes(bool isMOMEnts)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Types_Get] ").Append(objSqlServer.FormatSql(isMOMEnts));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }




        public static int InsertImageGallery(string imageFilename)
        {
            StringBuilder query = new StringBuilder("[BF_ImageGallery_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static SqlDataReader GetAllImageGallery()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_ImageGallery_Get] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetImageGallery(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_ImageGallery_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteImageGallery(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_ImageGallery_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }






        public static SqlDataReader GetRealDeals(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_RealDeals_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRealDeals()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_RealDeals_Get] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllRealDeals()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_RealDeals_Get] 1");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteRealDeals(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_RealDeals_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateRealDeals(int id, short type, string title, string imageFilename, bool isActive, string text, string activeDate, string expiryDate, string url)
        {
            StringBuilder query = new StringBuilder("[BF_RealDeals_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(type)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(expiryDate)).Append(",");
            query.Append(objSqlServer.FormatSql(url));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertRealDeals(short type, string title, string imageFilename, bool isActive, string text, string activeDate, string expiryDate, string url)
        {
            StringBuilder query = new StringBuilder("[BF_RealDeals_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(type)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(expiryDate)).Append(",");
            query.Append(objSqlServer.FormatSql(url));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static SqlDataReader GetRealDealsTypes()
        {
            StringBuilder query = new StringBuilder("[BF_RealDeals_Types_Get] ");
            SqlServer objSqlServer = new SqlServer();

            return objSqlServer.GetDataReader(query.ToString());
        }


        


        /**NOT USED YET **/

        public static SqlDataReader GetRssFeeds(int type)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[RssFeeds_GetFeeds] ").Append(type);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRssItems(int rssid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[RssFeeds_GetItems] ").Append(rssid);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRssItem(int itemid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[RssFeeds_GetItem] ").Append(itemid);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetEditorPicks()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[EditorPicks_Get]");

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static DataSet GetBabyName(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BabyNames_Get] ");

            query.Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataSet(query.ToString());
        }


        public static SqlDataReader GetAllQuestions()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Question_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataRow GetAllQuestions(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Question_GetOne] ");

            query.Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetFirstRow(query.ToString());
        }
        public static SqlDataReader GetQuestionAnswer(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Question_GetQuestionAnswers] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader DeleteQuestionAnswer(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Question_AnswerDelete] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAnswer(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Question_GetAnswer] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static int InsertAnswer(int questionID, string title, string link, bool active, bool IsSponsored)
        {
            StringBuilder query = new StringBuilder("[Question_InsertAnswer] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(questionID)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(link)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(IsSponsored));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void UpdateAnswer(int questionID, int id, string title, string link, bool active, bool IsSponsored)
        {
            StringBuilder query = new StringBuilder("[Question_UpdateAnswer] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(questionID)).Append(",");
            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(link)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(IsSponsored));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void MoveAnswerContent(int questionId, int answerId, bool moveup)
        {
            StringBuilder query = new StringBuilder("[Question_MoveAnswer] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(questionId)).Append(",");
            query.Append(objSqlServer.FormatSql(answerId)).Append(",");
            query.Append(objSqlServer.FormatSql(moveup));

            objSqlServer.ExecNonQuery(query.ToString());
        }

        public static SqlDataReader GetSettlementsRebates(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SettlementsRebates_GetOne] ").Append(id);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllSettlementsRebates()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SettlementsRebates_Get] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteSettlementsRebates(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SettlementsRebates_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertSettlementsRebates(string title, string url, string dateExpire, string settlementdeadline, bool isActiveAll, bool isActive, string text, string thumbnail, string name, string Eligible, string PotentialAward, string ProofPurchase, string ClaimFormDeadline)
        {
            StringBuilder query = new StringBuilder("[SettlementsRebates_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(settlementdeadline)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(Eligible)).Append(",");
            query.Append(objSqlServer.FormatSql(PotentialAward)).Append(",");
            query.Append(objSqlServer.FormatSql(ProofPurchase)).Append(",");
            query.Append(objSqlServer.FormatSql(ClaimFormDeadline));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void UpdateSettlementsRebates(int id, string title, string url, string dateExpire, string settlementdeadline, bool isActiveAll, bool isActive, string text, string thumbnail, string name, string Eligible, string PotentialAward, string ProofPurchase, string ClaimFormDeadline)
        {
            StringBuilder query = new StringBuilder("[SettlementsRebates_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(settlementdeadline)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(Eligible)).Append(",");
            query.Append(objSqlServer.FormatSql(PotentialAward)).Append(",");
            query.Append(objSqlServer.FormatSql(ProofPurchase)).Append(",");
            query.Append(objSqlServer.FormatSql(ClaimFormDeadline));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSettlementsRebates(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[SettlementsRebates_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            objSqlServer.ExecNonQuery(query.ToString());
        }


        public static SqlDataReader QuotesGetAll()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Quote_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataRow QuoteGetOne(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Quote_GetOne] ");

            query.Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetFirstRow(query.ToString());
        }
        public static SqlDataReader QuoteDelete(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Quote_Delete] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static int QuoteInsert(string quote, bool active)
        {
            StringBuilder query = new StringBuilder("[Quote_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(quote)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void QuoteUpdate(int quoteID, string quote, bool active)
        {
            StringBuilder query = new StringBuilder("[Quote_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(quoteID)).Append(",");
            query.Append(objSqlServer.FormatSql(quote)).Append(",");
            query.Append(objSqlServer.FormatSql(active));

            objSqlServer.ExecNonQuery(query.ToString());
        }

        public static SqlDataReader GetAllInternalAds()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[InternalAds_Get] ").Append("1,0");

            query.Append(',').Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetInternalAds(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[InternalAds_GetOne] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void MoveInternalAds(int id, bool moveup)
        {
            StringBuilder query = new StringBuilder("[InternalAds_Move] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(moveup));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteInternalAds(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[InternalAds_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteInternalAdsImage(int id)
        {
            StringBuilder query = new StringBuilder("[InternalAds_DeleteImage] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateInternalAds(int id, string title, string link, bool active, string imageFilename)
        {
            StringBuilder query = new StringBuilder("[InternalAds_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(link)).Append(",");
            query.Append(objSqlServer.FormatSql(active));


            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertInternalAds(string title, string link, bool active, string imageFilename)
        {
            StringBuilder query = new StringBuilder("[InternalAds_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(link)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(',');
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        /*   Sweepstakes & Contest   */

        public static SqlDataReader GetSweepContestsCategoryAll(bool isSample = false)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_GetAll] null,");
            query.Append(objSqlServer.FormatSql(isSample));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsCategoryOne(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteSweepContestCategory(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetSweepContests(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_GetOne] ").Append(id);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllSweepContests()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Get] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllSweepContestsByWebsite(bool isSample = false)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_GetWebsite] ");

            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isSample));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsCategory(int sweepcontestsid, bool isSample = false)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_GetAll] ");
            query.Append(sweepcontestsid);
            query.Append(",");
            query.Append(isSample);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteSweepContests(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSweepContests(int id, string title, string url, string nbPoints, string dateActive, string dateExpire, bool isActiveAll, bool isActive, string text, string imageFilename, string thumbnail, string name)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(nbPoints)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(dateActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSweepContestsThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSweepContests(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertSweepContests(string title, string url, string nbPoints, string dateActive, string dateExpire, bool isActiveAll, bool isActive, string text, string imageFilename, string thumbnail, string name, bool isSample = false)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(nbPoints)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(dateActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name)).Append(",");
            query.Append(objSqlServer.FormatSql(isSample));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void InsertPSweepContestsCategory(int sweepcontestsid, int category)
        {
            StringBuilder query = new StringBuilder("[SweepContestsToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(sweepcontestsid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int SweepContestsInsertCategory(string category, bool isSample = false)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Insert_Category] ").Append(objSqlServer.FormatSql(category)).Append(",").Append(objSqlServer.FormatSql(isSample));

            return int.Parse(objSqlServer.GetScalar(query.ToString()).ToString());
        }
        public static void SweepContestsUpdateCategory(int id, string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Update_Category] ").Append(objSqlServer.FormatSql(id)).Append(",").Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void InsertSweepContestsCategory(int pointsOffersid, int category)
        {
            StringBuilder query = new StringBuilder("[SweepContestsToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(pointsOffersid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void SweepContestsImageUpdateThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Image_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
    }
}