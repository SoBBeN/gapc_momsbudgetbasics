﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Unsub : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["email"]))
            {
                txtEmail.Text = Request.QueryString["email"];
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Validation v = new Validation();
        WebControl w;

        w = lblEmail;
        v.Email(txtEmail.Text, ref w);

        if (!v.HasError)
        {
            //string values = "api_key=0da2d62b_fdc3_4377_b8a2_9ac0ffb022f0&source_id=603&email=" + HttpUtility.UrlEncode(txtEmail.Text);
            //string url = "https://api2.all-inbox.com/?resource=unsubscribe";
            //string postResponse = null;
            //try
            //{
            //    postResponse = Functions.DoPost(ref url, ref values, null, null);
            //    if (!postResponse.Contains("200"))
            //    {
            //        ErrorHandling.SendException("Unsub All-Inbox CI", new Exception("Not 200 response"), postResponse + "<br /><br />" + values);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorHandling.SendException("Unsub All-Inbox CI", ex, txtEmail.Text);
            //}
            spnEmail.InnerText = txtEmail.Text;
            pnlForm.Visible = false;
            pnlSuccess.Visible = true;

            try
            {
                DB.DbFunctions.AddiContactUnsub(txtEmail.Text);
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("Unsub iContact CI", ex, txtEmail.Text);
            }
        }
    }
}