﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tips : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var page = Request.QueryString["page"];

        var mylink = "/tips";
        breadcrumbs1.AddLevel("Parenting Tips", mylink);



        if (page == null  || page == "") {

            StringBuilder sb = new StringBuilder();

            sb.Append("<p>Here are some simple and easy tips most parents can utilize:</p>");
            sb.Append("<p><a href=\"?page=Sleep\">Improve  Your Baby&rsquo;s Sleeping Habits.</a> Establishing your baby&rsquo;s sleeping habits is  important for everyone in the family.   Parents and babies alike benefit from a well-rested baby.  Many parents find this to be one of the more difficult  challenges of babyhood.  Depending on  your situation, you may want to try one of these tips for improving your baby&rsquo;s  sleeping habits.</p>");
            sb.Append("<p><a href=\"?page=Eat\">Your Baby&rsquo;s Eating Habits.</a> Every baby is unique in so many ways,  especially when it comes to eating.  Frankly,  even adults have complicated eating habits.   Here are some tips to consider when establishing your baby&rsquo;s eating  habits.</p>");
            sb.Append("<p><a href=\"?page=Cry\">Your Crying Baby.</a> Babies cry.  Crying is a way for babies to communicate  their needs to you.  It is sometimes  difficult to know what they want.  Are  they hungry, tired, sick or just wanting attention? Here are some tips to  consider when your baby is crying.</p>");
            sb.Append("<p><a href=\"?page=CollegeSavings\">Your Baby&rsquo;s College  Savings Plans. (already?)</a> It is never too early to start thinking about  saving for college.  Higher education is  getting more expensive each year.  The  time to start saving for your child&rsquo;s college education is now.  Here are some things to consider when  researching colleges savings plans.</p>");
            sb.Append("<p><a href=\"?page=Music\">Music for Babies?</a> Music has the potential to soothe, distract and  entertain babies.  But did you know music  also has the potential to enhance your child&rsquo;s mind? Consider these ideas for  exploring music for your baby.</p>");
            sb.Append("<p><a href=\"?page=Thumb\">Thumbsucking</a> Babies are born with the need to suck.  The intensity of this need varies from baby  to baby.  It is a normal behavior but  here are some thoughts concerning thumbsucking babies.</p>");
            sb.Append("<p><a href=\"?page=Equipment\">All that Baby  Equipment</a> Babies require a lot of equipment.  It is hard to know what and which kind to  get.  Here are some helpful guides to  help you determine what baby equipment you should get.</p>");
            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //Sleep tip
        if (page == "Sleep")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);

            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten Tips for  Improving Your Baby&rsquo;s Sleeping Habits</b></p>");
                sb.Append("<p>Establishing  your baby&rsquo;s sleeping habits is important for everyone in the family.  Parents and babies alike benefit from a well-rested  baby.  But for the most part, this is no  simple task! Many parents find this to be one of the more difficult challenges  of babyhood. </p>");
                sb.Append("<p>Depending  on your situation and needs, you may want to try a few of the following tips for  improving your baby&rsquo;s sleeping habits:</p>");
                sb.Append("<img class=\"baby\" src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/baby1.png\" />");
                   sb.Append("<ol>");
                        sb.Append("<li><b>Every family has  different needs</b>.  This is the most important tip.  Sleeping habits are very personal.  Consider your families&rsquo; overall sleeping  needs and pay close attention to your baby&rsquo;s.</li>");
                        sb.Append("<li><b>Each baby&rsquo;s sleeping  habits will vary to a degree</b>.  Some  babies want some extra cuddling right before bed, while others need very  little.  And just because your neighbor&rsquo;s  baby falls asleep within minutes doesn&rsquo;t mean yours will.</li>");
                        sb.Append("<li><b>If the parents are  calm, the baby is calm.</b>  Of course this is  not a perfect science, but if you are stressed there is a good chance your baby  will sense it.  Remaining calm and  peaceful during your baby&rsquo;s bedtime routine can help create better sleeping  habits.</li>");
                        sb.Append("<li><b>Maintain a consistent  bedtime routine</b>.  Your baby&rsquo;s sleeping habits will benefit from  a consistent bedtime routine: dinner, bath, quiet time, bed.</li>");
                        sb.Append("<li><b>Good sleeping habits  begin with a regular bedtime.</b>  Try  to keep your baby on a regular schedule that does not vary too much from day to  day.</li>");
                        sb.Append("<li><b>Quiet time before  bedtime is essential</b>.  Try not to turn on the T.V. too close to  bedtime.  The noise and images will not  help to settle your baby down.  Read a  book or sing a song to your baby instead.</li>");
                        sb.Append("<li><b>Massage your baby at  bedtime</b>.  By taking a few minutes to add this to your  baby&rsquo;s bedtime routine, you will help to enhance your baby&rsquo;s sleeping habits.</li>");
                        sb.Append("<li><b>Use a white-noise  machine</b>.  Some parents find that white-noise -- a constant  soft sound that blocks out other noises -- helps to create better sleeping  habits for their babies, especially if there are noisy siblings around.</li>");
                        sb.Append("<li><b>Keep your baby on a  regular nap schedule</b>.  It has been my experience that &ldquo;sleep begets  sleep.&rdquo;  One of the best ways to create  better sleeping habits is to get your baby into the habit of napping daily.</li>");
                        sb.Append("<li> <b>Avoid  baby falling asleep in the car or stroller</b>.   A baby that gets into the habit of falling asleep while in motion may  have greater difficulty falling asleep in bed while lying still.</li>");
                   sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //Eat tip
        if (page == "Eat")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten  Tips for Baby&rsquo;s Eating Habits</b></p>");
                sb.Append("<p>Every  baby is unique in so many ways.  While  eating habits will vary from baby to baby, here are some tips to consider when establishing  your baby&rsquo;s eating habits:</p>");
                sb.Append("<img class=\"baby\"  src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/baby2.png\" />");
            sb.Append("<ol>");
                    sb.Append("<li>Eat healthy while you are pregnant.  There is  research to suggest that your baby&rsquo;s eating habits may begin during pregnancy.</li>");
                    sb.Append("<li>If  you are able and inclined to, consider breastfeeding your baby.  Many organizations cite breastfeeding as the  best source of nutrition for babies and a terrific way to create healthy eating  habits.</li>");
                    sb.Append("<li>For  babies that eat solid foods, offer a wide variety of healthy choices.  The more choices the better!</li>");
                    sb.Append("<li>Be  a good example.  Many babies imitate  their parents&rsquo; or caregiver&rsquo;s eating habits.   Try to incorporate healthy foods into your diet as well. </li>");
                    sb.Append("<li>Try  to maintain a consistent mealtime routine.   Your baby&rsquo;s eating habits will benefit from regular healthy meals.  </li>");
                    sb.Append("<li>Avoid  too many in between meal snacks, especially unhealthy ones.  Too much snacking can interfere with your baby&rsquo;s eating habits.  </li>");
                    sb.Append("<li>Try  to avoid fast food for as long as possible.   A baby should not get into the habit of eating fast food.  There will be plenty of time for that later.</li>");
                    sb.Append("<li>Limit  sweet drinks or avoid them altogether.  A  baby who gets into the habit of having sugary drinks may refuse healthier  choices, such as milk and water.</li>");
                    sb.Append("<li>Offer  your baby a balance of foods.  Good  eating habits include portions of protein, dairy, carbohydrates, fruits and  vegetables.</li>");
                    sb.Append("<li>Try  not to compare one baby&rsquo;s eating habits to another.  Each baby&rsquo;s eating habits will vary.  Respect your baby&rsquo;s healthy food choices as  well as the amount she wants to eat.  </li>");
              sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //Cry tip
        if (page == "Cry")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten  Tips for Responding to Your Crying Baby</b></p>");
            sb.Append("<p>Babies  cry.   Crying is a way for babies to  communicate their needs to you.  It is  sometimes difficult to know what they want.   Here are some tips to consider when your baby is crying:</p>");
            sb.Append("<img class=\"baby\"  src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/Crying baby.jpg\" />");
            sb.Append("<ol>");
                    sb.Append("<li>Your  crying baby may be hungry!  Unless you  have just fed your baby, this should be the first step in determining why your  baby is crying.</li>");
                    sb.Append("<li>Babies  often cry when their diaper is soiled.  </li>");
                    sb.Append("<li>Babies  sometimes cry when coming down with an illness.   Determine if your crying baby feels feverish  or has the sniffles or other symptoms of an illness.</li>");
                    sb.Append("<li>Your  crying baby may be over stimulated.  Many  babies cry when they have had too much exposure to noise, people and/or visual  activity (T.V.).</li>");
                    sb.Append("<li>Some  babies cry when they are overtired.   Consider that your baby may be tired and need some extra rest.</li>");
                    sb.Append("<li>Some  crying babies need extra cuddling and attention.  Placing your baby in a sling or other carrier  on your body may provide the right amount of comfort to soothe and quiet your  crying baby. <a href=\"http://www.cyh.com/HealthTopics/HealthTopicDetails.aspx?p=114&amp;np=141&amp;id=1829\">http://www.cyh.com/HealthTopics/HealthTopicDetails.aspx?p=114&amp;np=141&amp;id=1829</a></li>");
                    sb.Append("<li>Check  to see if your crying baby is too hot or too cold.</li>");
                    sb.Append("<li>Some  crying babies may be experiencing colic.</li>");
                    sb.Append("<li>Try  to soothe your crying baby.  Consider  what calms your baby, for example a pacifier, motion (stroller or car ride), a favorite  blanket, or a massage.</li>  ");
                    sb.Append("<li>Stay calm! It is easy to become stressed when a baby cries for a long period of time. Remaining as calm as possible will help.</li>");
                sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //CollegeSavings tip
        if (page == "CollegeSavings")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten  Tips on College Savings Plans</b></p>");
            sb.Append("<p>Higher  education is getting more expensive each year.   The time to start saving for your child&rsquo;s college education is now.  Here are some tips to consider when  researching savings plans.</p>");
                sb.Append("<ol>");
                    sb.Append("<li>Parents  should consider starting a savings plan as soon as possible after their child  is born.</li>");
                    sb.Append("<li>Take  a look at the cost of college tuition before determining a college savings plan  so that you may plan efficiently.</li>");
                    sb.Append("<li>There  are many college savings plan options available.</li>");
                    sb.Append("<li>There  are many advantages to investing in an Internal Revenue Code Section 529  savings plan (also known as a Qualified Tuition Program).</li>");
                    sb.Append("<li>There  are fees and expenses associated with the 529 savings plan.</li>");
                    sb.Append("<li>Any  money invested under the 529 savings plan must be used for higher education  purposes.</li>");
                    sb.Append("<li>There  are federal tax incentives associated with college savings plans.</li>");
                    sb.Append("<li>Some  people advise consulting a professional when putting together a college savings  plan.</li>");
                    sb.Append("<li>By  investing in a college savings plan, you will be reducing the reliance on debt  later on.</li>");
                    sb.Append("<li>There  are many challenges associated with applying to colleges; initiating a savings  plan early on will help at least to reduce the financial stress.</li>");
                sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //Music tip
        if (page == "Music")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten  Tips on Music for Babies</b></p>");
            sb.Append("<p>Music  has the potential to soothe, distract and entertain babies.  Here are some tips for exploring music for  your baby.</p>");
                sb.Append("<ol>");
                    sb.Append("<li>Playing  music for babies has been linked to early literacy development.</li>");
                    sb.Append("<li>Music  has the potential to calm and soothe an agitated baby. </li>");
                    sb.Append("<li>Premature  babies who listen to music have exhibited proven health benefits.</li>");
                    sb.Append("<li>The  earlier you introduce music to your baby, the better.</li>");
                    sb.Append("<li>Enroll  your baby in an age-appropriate music class.   There are many options to choose from.  </li>");
                    sb.Append("<li>Babies,  together with parents or caregivers, may even take a music class online.</li>");
                    sb.Append("<li>Play  music for your baby while driving in the car.   A fussy baby in the back seat will likely quiet down when her favorite  song is played.</li>");
                    sb.Append("<li>Many  babies enjoy the Baby Einstein music products.</li>");
                    sb.Append("<li>Raffi  is another popular musical artist for babies and young children.</li>");
                    sb.Append("<li>If  you don&rsquo;t have music created specifically for babies, try playing whatever you  prefer (not too loudly), there is a good chance your baby will like it  too.    </li>");
                sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //Thumb tip
        if (page == "Thumb")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten  Tips for Thumbsucking</b></p>");
            sb.Append("<p>Babies  are born with the need to suck.  The  intensity of this need varies from baby to baby.  Here are some tips concerning thumbsucking:</p>");
                sb.Append("<ol>");
                    sb.Append("<li>Allow  your baby to suck.  Babies suck their  thumbs to comfort and soothe themselves.</li>");
                    sb.Append("<li>Try  to offer your baby the pacifier instead.   It is more difficult to stop a baby from thumbsucking; the pacifier can  be taken away.  </li>");
                    sb.Append("<li>Make  sure your baby&rsquo;s hands are clean.     </li>");
                    sb.Append("<li>Many  babies will stop sucking their thumbs by six or seven months.</li>");
                    sb.Append("<li>Ignore  your babies&rsquo; thumbsucking.  Do not make it  a big issue.</li>");
                    sb.Append("<li>Give  your baby or child praise when she is not thumbsucking. </li>");
                    sb.Append("<li>The ADA  says children should stop thumbsucking by the time the permanent front teeth  are ready to erupt.</li>");
                    sb.Append("<li>When  trying to give up thumbsucking, do not scold your baby or child for sucking her  thumb.</li>");
                    sb.Append("<li>Keep  a chart of thumbsucking behavior to understand when and why your baby or child  sucks his thumb.</li>");
                    sb.Append("<li>Replace thumbsucking with other comforts to reduce the  amount of, and need for, thumbsucking.    </li>");
                sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }
        //Equipment tip
        if (page == "Equipment")
        {
            mylink = "/tips?page=" + page;
            breadcrumbs1.AddLevel(page, mylink);
            StringBuilder sb = new StringBuilder();
            sb.Append("<p><b>Ten  Tips for Selecting Baby Equipment</b></p>");
            sb.Append("<p>Babies  require a lot of equipment.  Here are  some tips to help determine what type of baby equipment you will need.</p>");
                sb.Append("<ol>");
                    sb.Append("<li>If  you drive, choosing an appropriate car seat is an essential part of your baby&rsquo;s  equipment. <a href=\"http://www.mysimon.com/Consumer-Reports-Car-Seats/4002-9375-6310830.html\">http://www.mysimon.com/Consumer-Reports-Car-Seats/4002-9375-6310830.html</a></li>");
                    sb.Append("<li>Another  important piece of baby equipment is a high chair for feeding.  <a href=\"http://blogs.consumerreports.org/baby/\">http://blogs.consumerreports.org/baby/</a></li>");
                    sb.Append("<li>Some  parents will want to add a bassinet to their baby&rsquo;s equipment inventory.  <a href=\"http://www.thenewparentsguide.com/baby-bassinets-1.htm\">http://www.thenewparentsguide.com/baby-bassinets-1.htm</a></li>");
                    sb.Append("<li>Selecting  a crib for a growing baby to sleep in is essential.   <a href=\"http://www.kidshealth.org/parent/firstaid_safe/home/products_cribs.html\">http://www.kidshealth.org/parent/firstaid_safe/home/products_cribs.html</a></li>");
                    sb.Append("<li>Although  it is a fairly large piece of equipment, babies can safely play or sleep in a  &ldquo;playpen&rdquo; or &ldquo;portable crib.&rdquo;    <a href=\"http://www.kidshealth.org/parent/firstaid_safe/home/products_playpens.html\">http://www.kidshealth.org/parent/firstaid_safe/home/products_playpens.html</a></li>");
                    sb.Append("<li>To  hear baby from another room, many parents invest in equipment known as a &ldquo;baby  monitor.&rdquo;</li>");
                    sb.Append("<li>To  soothe an agitated baby or for the convenience of walking &ldquo;hands free,&rdquo; many  caregivers add a baby sling or carrier to their equipment list. <a href=\"http://www.kidshealth.org/parent/firstaid_safe/travel/products_backpacks.html\">http://www.kidshealth.org/parent/firstaid_safe/travel/products_backpacks.html</a></li>");
                    sb.Append("<li>Your  baby equipment will not be complete without a stroller that suits your baby&rsquo;s size. <a href=\"http://www.kidshealth.org/parent/firstaid_safe/home/products_playpens.html\">http://www.kidshealth.org/parent/firstaid_safe/home/products_playpens.html</a></li>");
                    sb.Append("<li>Many  parents add a changing table to their baby equipment list. <a href=\"http://www.kidshealth.org/parent/firstaid_safe/travel/products_changing_tables.html\">http://www.kidshealth.org/parent/firstaid_safe/travel/products_changing_tables.html</a></li>");
                    sb.Append("<li>A  &ldquo;bouncy seat&rdquo; is an alternative piece of equipment in which babies may play or  feed.                  </li>");
                sb.Append("</ol>");

            litTips.Text = sb.ToString();
            sb.Clear();
        }



    }
}