﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class NewsInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("News", "/News/");

        SqlDataReader dr;

        int id;
        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = -1;
        }

        dr = DB.DbFunctions.GetNewsOne(id);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int i = 0;
                StringBuilder sb = new StringBuilder();
                dr.Read();

                string link = String.Format("<a href=\"{0}\" target=\"_blank\">", dr["WebSiteLink"]);
                string newsurl = "News/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Convert.ToString(dr["Thumbnail"]);
                string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 220).Replace("\"", " ").Trim();
                string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 220).Replace("\"", " ").Trim();

                sb.Append("<div class=\"new\">");
                sb.AppendFormat("<div class=\"img\"><img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"], Functions.ConvertToString(dr["ImageFilename"]), Convert.ToString(dr["Title"]));
                sb.Append("<div class=\"description\">");
                sb.AppendFormat("<h2 class=\"title\">{0}</h2>", Functions.ConvertToString(dr["Title"]));
                sb.Append(Functions.RemoveHtml(Convert.ToString(dr["Text"])));
                sb.AppendFormat("<div class=\"button\">{0}Read More on {1}</a></div>", link, dr["WebSiteName"]);
                sb.Append("</div>");

                sb.Append("</div><div style=\"clear:both;\"></div>");

                litCoupons.Text = sb.ToString();

                ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                ((ITmgMasterPage)Master).PageURL = newsurl;
                ((ITmgMasterPage)Master).PageType = "article";
                ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);

                fbComments1.Href = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "News/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
            }
            dr.Close();
            dr.Dispose();
        }

        dr = DB.DbFunctions.GetNewsPrevNext(id);
        if (dr != null)
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    HtmlAnchor lnk;
                    HtmlAnchor imgLnk;
                    HtmlImage img;

                    if (Convert.ToString(dr["Type"]) == "Prev")
                    {
                        lnk = lnkPrev;
                        imgLnk = imgLnkPrev;
                        img = imgPrev;
                    }
                    else
                    {
                        lnk = lnkNext;
                        imgLnk = imgLnkNext;
                        img = imgNext;
                    }

                    img.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                    lnk.HRef = "/News/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    imgLnk.HRef = lnk.HRef;
                }
            }
            dr.Close();
            dr.Dispose();
        }
    }
}