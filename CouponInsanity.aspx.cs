﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CouponInsanity : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {

        breadcrumbs1.AddLevel("Coupon Insanity", "/CouponInsanity/");

        string url = "http://syndication.impactradius.com/xml-ad/38615/56160/758";
        RssManager rm = null;

        Retries retry = new Retries(3);
        while (retry.NeedsRetry())
        {
            try
            {
                rm = new RssManager(url);
                rm.GetFeed();
            }
            catch (Exception ex)
            {
                retry.HandleException(ref ex, "GetFeed", "URL: " + url, true);
            }
        }
        if (retry.BeenSuccessfull())
        {
            StringBuilder sb = new StringBuilder();
            if (rm.RssItems.Count == 0)
                sb.Append("No items found");

            int i = 0;
            int start = 0;
            if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
            {
                lnkPrev.HRef = "/CouponInsanity/?s=" + (start - NB_PER_PAGE).ToString();
                lnkPrev.Visible = true;
            }

            lnkNext.HRef = "/CouponInsanity/?s=" + (start + NB_PER_PAGE).ToString();
            foreach (Rss.Items rs in rm.RssItems)
            {
                string content = rs.Description;
                if (content != null)
                {
                    if(start == 0)
                    {
                        i += 1;

                        if (i > NB_PER_PAGE)
                            break;

                        string link = String.Format("<a target=\"_blank\" href=\"{0}\">", rs.Link);
                        content = content.Replace("<div xmlns=\"http://www.w3.org/2005/Atom\"><img", String.Format("<div class=\"coupon\"><div class=\"img\">{0}<img", link));
                        content = content.Replace("></img>", " /></a></div>");
                        content = content.Replace("<div class=\"full_details\">", String.Format("<div class=\"details details_pad\"><div class=\"title\">{0}", link));
                        content = content.Replace("</div></div><div class=\"expiration\">", "</a><div class=\"expire\">");
                        sb.Append(content);
                        sb.Append("</div><div style=\"clear:both;\"></div><br /><hr class=\"blueline\" />");
                        //if (i % 3 == 0)
                        //{
                        //    if (Request.Browser.IsMobileDevice)
                        //    {
                        //        sb.Append("<div class=\"ad\">");
                        //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                        //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                        //    }
                        //}
                    }
                    else
                    {
                        start--;
                    }
                }
            }
            litCoupons.Text = sb.ToString();

            //if (i < 3)
            //{
            //    if (Request.Browser.IsMobileDevice)
            //    {
            //        sb.Append("<div class=\"ad\">");
            //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
            //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
            //    }
            //}

            if (i > NB_PER_PAGE)
                lnkNext.Visible = true;
        }
        else
        {
            litCoupons.Text = "URL failed";
        }

        SqlDataReader dr;
        dr = DB.DbFunctions.GetContent(102);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                    litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                    if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                    {
                        if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                            litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                        else
                            litImage.Text = "<a>";

                        litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                        litImage.Text += "</a>";
                    }
                    else
                        litImage.Visible = false;
                }
            }

            dr.Close();
            dr.Dispose();
        }


    }
}