﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RecipeCategory : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Recipe Categories", "/Recipes/");

        litcategory.Text = Convert.ToString(Page.RouteData.Values["category"]);

        int id;
        string categoryType = string.Empty;
        string category = string.Empty;

        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out id))
        {
            if (int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
            {
                categoryType = Convert.ToString(Page.RouteData.Values["categoryType"]);
                category = Convert.ToString(Page.RouteData.Values["category"]);
            }
            else
                id = -1;
        }
        else
        {
            categoryType = Request.QueryString["categoryType"];
            category = Request.QueryString["category"];
        }

        category = category.Replace("_", " ");
        litcategory.Text = category;

        SqlDataReader dr;
        StringBuilder sb;

        if (id > 0)
        {
            //((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            breadcrumbs1.AddLevel(category, "Recipes/" + id + "/" + Functions.StrToURL(categoryType) + "/" + Functions.StrToURL(category) + "/");
            ((ITmgMasterPage)Master).PageTitle = category;
            string pageurl = "Recipes/" + id + "/" + Functions.StrToURL(categoryType) + "/" + Functions.StrToURL(category) + "/";
            ((ITmgMasterPage)Master).PageURL = pageurl;
            ((ITmgMasterPage)Master).PageType = "article";
            ((ITmgMasterPage)Master).PageDescription = category;

            dr = DB.DbFunctions.GetRecipeByCategory(categoryType, id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int start = 0;
                    if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                    {
                        lnkPrev.HRef = pageurl + "?s=" + (start - NB_PER_PAGE).ToString();
                        lnkPrev.Visible = true;
                    }

                    lnkNext.HRef = pageurl + "?s=" + (start + NB_PER_PAGE).ToString();
                    while (start-- > 0 && dr.Read()) ;

                    sb = new StringBuilder();
                    int i = 0;
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string link = "<a href=\"/Recipe/" + id.ToString() + "/" + categoryType + "/" + Convert.ToString(Page.RouteData.Values["category"]) + "/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "\">";

                        sb.Append("<div class=\"lifestyle\">");
                        sb.Append(link).AppendFormat("<div class=\"img recipe\"><img src=\"{0}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["ImageFilename"]));
                        sb.Append("<div class=\"description\">");
                        sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", Functions.ConvertToString(dr["Title"]), link);
                        sb.Append("</div>");

                        sb.Append("</div><div style=\"clear:both;\"></div><br /><hr />");
                        //if (i % 3 == 0)
                        //{
                        //    if (Request.Browser.IsMobileDevice)
                        //    {
                        //        sb.Append("<div class=\"ad\">");
                        //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                        //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                        //    }
                        //}
                    }

                    //if (i < 3)
                    //{
                    //    if (Request.Browser.IsMobileDevice)
                    //    {
                    //        sb.Append("<div class=\"ad\">");
                    //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                    //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    //    }
                    //}

                    litOtherRecipes.Text = sb.ToString();

                    if (i > NB_PER_PAGE)
                        lnkNext.Visible = true;
                }
                dr.Close();
                dr.Dispose();
            }

        }
    }
}