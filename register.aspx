﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageRegister.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        
        <!-- Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" />
      	<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600" rel="stylesheet" />
      
      	<!-- Font Awesome -->
      	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" />
      	<!-- Font Awesome -->
      	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" />
            <!-- Bootstrap CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" />
            <!-- Animate.css -->
        <link href="/css/animate.css" rel="stylesheet" />
        <!-- Custom CSS -->
      	<link href="/css/base.css" rel="stylesheet" />

        	<!-- jQuery -->           
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
      	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
           <!-- jquery-Validate -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
            <script type="text/javascript" src="/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="/js/moment.js"></script>


   <style>
            
body {
        background-image: url(../images/MBB_background.png);
  background-position: center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
    background-attachment: fixed;
    background-repeat: no-repeat;
    
}


    /* Custom, iPhone Retina */ 
    @media only screen and (min-width : 320px) {
        #acgLogo {    
            width: 80px;
            padding-top: 6%;
        }
        h2 {
            font-size: 1.0em;
            font-family: 'Kanit', sans-serif;
            color: #ffffff;
        }
        body {
            margin-bottom: 62px !important;
        }
    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (min-width : 480px) {
        #acgLogo {    
            width: 80px;
            padding-top: 6%;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        #acgLogo {
            width: 150px;
            padding-top: 2%;
        }
        h2 {
            font-family: 'Open Sans', sans-serif;
            color: #ffffff;
            font-size: 1.5em;
        }
    }

       	</style>
        <script>
         $(document).ready(function () {
        $('#txt_phone').mask('(000) 000-0000');
      });
  </script>

      
 
        <style>         
          
    .error {
            color: red;
            border: unset !important;
            text-align: left;
            font-size: 14px;
            font-family: sans-serif;
            font-weight: 500;
        }
   .form-control {
      padding: 1rem;
      box-shadow: 0px 0px 4px black;
      width: 95%;
      margin: auto;
      margin-top: 16px;
   }       
          
    .subcopy {
    text-shadow: 0px 3px 6px #000000;
    padding-bottom: 0;
    font-size: 44px;
    color: #FFFFFF;
    font-family: 'kanit','Open Sans', sans-serif;
    font-weight: 600;
    text-align: center;
    line-height: 1.1;
    margin-bottom: 0;
    }
     
    #subtitle {
      font-size: 13px;
      font-weight: 500;
      margin-top: 0px;
      margin-bottom: 20px; 
      text-align: center; 
      color: #8f8f8f;
      font-family: 'kanit','Open Sans', sans-serif;
    }
    .logo 
    {
      text-align: center;
    }
img#logo {
        max-width: 100%;
    padding-top: 2%;
}
  footer.footer {
    margin-top: 10%;
    color: #8f8f8f;
    font-family: 'Kanit','Open Sans', sans-serif;
    font-weight: 300;
        text-align: center;
}
  footer .legal{
  	width: 95%;
    margin: 0 auto 10px;
    font-weight: 300;
    line-height: 1.1;
    font-size: 14px;
  }
    footer.footer a {
      font-family: 'Kanit','Open Sans', sans-serif;
    font-weight: 300;
    color: #8f8f8f;
}
                 
/*question css start*/
   .PFG_SingleLayout_1203 .sweeps{
    display:none;
   }     
   .PFG_SingleLayout_1203 .form_bg {
    background: rgba(255, 255, 255, 0.6);
    padding: 3%;
    width:95%;
    max-width:700px;
    margin-top: 2%;
    margin-bottom: 2%;
    display: inline-block;
    border-radius: 15px;
}   
.PFG_SingleLayout_1203 .coreg_buttons > a {
  color:white;
  background-color: #49882C; 
  text-decoration:none;
  font-family: 'kanit','Open Sans', sans-serif;
  width: calc(50% - 30px);
  padding: 15px;
  transition-duration: 0.4s;
}
.PFG_SingleLayout_1203 .coreg_buttons > a:hover {
          box-shadow: 0px 3px 20px #1c1c1c;
}
.PFG_SingleLayout_1203 .coreg_question {
	font-family: 'kanit','Open Sans', sans-serif;
    font-weight: 600;
    font-size: 25px;
}
.PFG_SingleLayout_1203 .draw_footer_container {
  	font-family: 'kanit','Open Sans', sans-serif;
    color: #ffffff;
    margin-top: 12%;
    color: black;
}
.dob_error {
            display: none;
    color: #a94442;
    font-size: 12px;
            }
          
/*question css end*/
@media(max-width:768px){
  .subcopy {
    font-size: 35px;
  }
  #subtitle {
    font-size: 10px;
  } 
  .PFG_SingleLayout_1203 .coreg_buttons > a {
    width: calc(50% - 10px);
	}
    .PFG_SingleLayout_1203 .coreg_buttons > a {
      margin: 10px 5px;
	}
}
@media (max-width:500px) { 
    	footer.footer {
    margin-top: 5%;
}
    footer .legal {
    font-size: 12px;
}
    footer.footer a {
     font-size: 12px;
	}
   .subcopy {
     font-size: 25px;
   }
} 
          </style>
      
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">

     <header>
          
        <div class="container">
            <div class="logo text-center">
                <img src="images/mbb_logo_small.png" id="logo" />
          </div>
       </div>
       <div class="container" id="head">
              <h1 class="questions subcopy" id="head">SIGN UP FOR THE ULTIMATE SAVVY MOMS' RESOURCE GUIDE!</h1>
    <h2 id="subtitle">
        Explore Mom’s Budget Basics to discover the best Budget Ideas, Free Samples, Recipes, News & More!
<br />
Receive daily text alerts regarding account updates, new deals, and offers for moms.</h2>
         <br />
        </div>
    
          
        </header>
    <style>
    #skipButton{
   display:none;
  }
   footer .legal {
  	display:none;
  }
   .form_container
  {
    background: rgba(255, 255, 255, 0.6);
    margin-right: auto;
    margin-left: auto;
    margin-top: 0%;
    width: 95%;
    max-width: 460px;
    border-radius: 20px;
    padding:20px 10px;

  }
    #consent{
        padding-right:20px;
    }
    
    .disclaimertext{
    text-align: center;
    font-size: 9pt;
    padding-top: 10px;
    }
   .form-control {
    padding: 1rem;
    box-shadow: 0px 0px 4px black;
    width: 95%;
    max-width: 360px;
    margin: auto;
    margin-top: 16px; 
}
     #txt_email {
    margin-top: 0px; 
}
   Select {
padding: 1rem;
    box-shadow: 0px 0px 4px black;
    margin: auto;
    margin-top: 16px;
    width: calc(33% - 3px);
  }

   .selectWrapper{
      width: 95%;
    max-width: 360px;
    margin: auto;
  }
 input#btnSubmit {
background-color: #4711b4;
    color: #ffffff;
    font-family: 'Open Sans', sans-serif;
    font-weight: 700;
    font-size: 36px;
    border-color: #1075c1;
    text-align: center;
    display: block;
    max-width: 360px;
    width: 95%;
    margin: 10px auto;
}
     #disclaimer_1 {
        float: left;
        margin-top: 5%;
     }

  @media(max-width:500px){
  	 input#btnSubmit {
    	font-size: 27px;
	}
     .form-control {
    	padding: .75rem;
	}
     Select {
    	padding: .75rem;
    }
  }
    @media(max-width:375px){
  	 input#btnSubmit {
    	font-size: 24px;
	}
  }
</style>

 
<div class="container">
<!-- multistep form -->
<div class="form_container uniform_margin">
<form id="msform" class="signupForm" method="post" action="/thankyou.aspx">
    <div class="inner">
                            <div class="input_wrapper_outer">
                                <div class="input_wrapper">
  <!--Needed-->
  <input type="hidden" name="remotePenCriteriaPassed" id="remotePenCriteriaPassed" value="false" />
  <input type="hidden" name="zip_lookup_done" id="zip_lookup_done" value="" />
  <input type="hidden" name="remotePenCheckEmail" id="remotePenCheckEmail" value="" />

  <!-- fieldsets -->
  <fieldset>
   
<%--    <div class="inner_input">
    <input type="text" name="email" id="txt_email" value="" placeholder="Email" class="form-control" autofocus="autofocus" required />
    <div class="help-block with-errors text-center">Please enter your email</div>
    </div>--%>
      
    <div class="inner_input">
    <input type="text" name="first_name" id="txt_first_name" value="" placeholder="First Name" class="form-control" />
    </div>
      <%--      
   	<div class="inner_input">
    <input type="text" name="last_name" id="txt_last_name" value="" placeholder="Last Name" class="focus form-control" required/>
      <div class="help-block with-errors text-center">Please enter your last name</div> 
    </div>
    <div class="inner_input">
    <input type="tel" name="zip" id="txt_zip" value="" placeholder="Zip Code" class="focus form-control" required />
    <div class="help-block with-errors text-center">Please enter a valid zip code</div>
    </div>
    <div class="inner_input">
    	<input type="text" name="address_1" id="txt_address" value="" placeholder="Address" class="focus form-control" required />
    	<div class="help-block with-errors text-center">Please enter a valid address</div>
    </div>
    <div class="inner_input">
      <div class="selectWrapper">
      <select name="dob_m" id="ddl_dob_month" class="slide_dob">
        <option selected="selected" value="">Date</option>
        <option value="01">Jan</option>
        <option value="02">Feb</option>
        <option value="03">Mar</option>
        <option value="04">Apr</option>
        <option value="05">May</option>
        <option value="06">Jun</option>
        <option value="07">Jul</option>
        <option value="08">Aug</option>
        <option value="09">Sep</option>
        <option value="10">Oct</option>
        <option value="11">Nov</option>
        <option value="12">Dec</option>
      </select>

      <select name="dob_d" id="ddl_dob_day" class="slide_dob">
        <option selected="selected" value="">Of</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option> 
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
      </select>
      <select name="dob_y" id="ddl_dob_year" class="slide_dob"></select>
        </div>
      </div>
       <div class="inner_input">
      <input type="tel" name="dob" class="focus" style="display:none;" id="hdn_dob" value="" placeholder="(mm/dd/yyyy)" required />
      <div class="help-block with-errors dob_error text-center">Please enter a valid birthday</div>  
      
    </div>--%>
    <div class="inner_input">
    	<input type="tel" name="phone" id="txt_phone" value="" placeholder="(123) 456-7890" class=" form-control" />
    </div>  
        <div class="row">
  	<div class="col-lg-12">
          <input type="checkbox" name="checkbox_1" id="disclaimer_1" value="" />
           <asp:Literal runat="server" ID="litDisclaimer1"></asp:Literal>
          <div class="checkbox_wrapper"></div>
                                      <br />
                                      <asp:Literal runat="server" ID="litDisclaimer2"></asp:Literal>
              </div>
  </div>
  
<%--<div class="modal fade" id="partnersModal" tabindex="-1" role="dialog" aria-labelledby="marketingPartners" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="partnersModalLabel">Marketing Partners</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size: 11px;">
        Time 2 Read, Omni Research, jointpainreliefgroup.com, Loud Cloud Nine, The-Solar-Project.com, Debt.com, Diabetic Connect, Skyline Health Services, MyBundleSaver, Q3 Insurance Solutions, Automotive Services Center, Prestige Magazine Service, E Social Marketing, LLC, Assurance IQ Inc, Better Health Kare, Consumer Awareness Group, Debt Help Express, Nationwide Student Loan, All Finance 411, Credit Lair, TargetedCareer, Homebidz, Credit Fix, Best Rate Referrals, Alliance Home Security, Home Services Companies, Solar Advocacy Group, Solar Research Group, Solar Savings Center, Solar Source Group, Touchstone, Sweepstakes Entry Center, Mortgage Advisor, Verde Energy USA, Sealed Inc, Sprint, Instant Platinum Offers, Injury-Review, Direct Energy Services, Home Biz Search, Pillpack, Student Payment Relief, ReadyForMyQuote, Leadcreations.com, Affinity Marketing Partners, Anthem Tax, Precision Mortgage, Health Benefits One, Rick Case KIA, Tax Defense Network, Optima Tax, Community Tax, Intellectual Inc, Buyers Inc, TaxInvestigators.com, Tax Inc, Outsource Inc, Advertising Inc, Alumni Aid, NCAC, CSADVO, Champion Savings LLC., The Brace Doctor, DeletePoorCredit.com, Nationwide Resources, BankruptcyHelpers.org, Helping Hands Association, Royal Seas Cruises, Support First, Great American Readers, 247 Paydayloan Center, Drips.com, My Savings Alliance, Magazine Publisher Service, HomeBusinessFortune, All American Readers, Nature Coast Publications, Suretouchleads, Leadsrow, Vehicle Protection Center, Car Guardian, Ringchat, Lexington Law, Credit Fix, Health Right, Injury Attorneys Network, Wireless Medical Alert, Soleo Communications, Inc., Shop Your Way, Diabetic Discount club, Rxtome, Backpain Discount Club, Festive Health Backbrace Partners, Preferred Guest Resorts, Job Resource Center, JRC, Life Line Screening, OneTouch Direct, White & Twombly, P.A., Optimum Medical Supply, Easy Rest Beds, Help Advisors, Verus Healthcare, e-TeleQuote Insurance, Inc, Certainty Auto Protection, Roadstead Auto Protection, Brobiz LLC, Advantage Auto Protection, Everlast Home Energy Solutions, Active Home Marketing, Home Source Marketing, Eximia Health, Integrated Sales Solutions, LLC, Soleo Communications, Inc., Flatiron Media, Bankruptcy Attorneys Network, Pure Talk USA, Community Tax LLC, United Medicare Advisors, Senior Market Quotes, Open Market Quotes, Spring Insurance Solutions, TrueChoice Insurance Services, DR Media LLC, Education ProNetwork, Settlement Marketing Group, LLC, Paperback, LLC, Paperback Services, LLC, CDSC, LLC, GoCDSC, LLC, Spanish Leads, LLC, Tax Investigators LLC, Tax Inc, DebtRevision.com, AlumniAid.org, Advertising Inc, Callbids.com, Outsource LLC, Health Source Network, Help Advisors, Cege Media LLC, Oxford Tax Partners, EM Credit Corp, Advocator Group, Americas Debt Choice, Consolidated Credit Counseling Services, Lighthouse Finance Solution, Telcare, Fastflow, Forward Leap Marketing, Western Benefits Group, Viva Medical Supply, Arbor Medical, Modern Medical, Champion Medical, Universal Healthcare Group, Four Seasons Oxygen, Hogan P&O, Disability Advisor, CCCF (Credit Counseling), Solid Quote LLC, Edata By Design, Inc., SolidQuote LLC, Disability Help Center, Shore Marketing Group, TaxLeads.com, Fidelity Tax Relief, Silver Tax, Crisp Connections, Legacy Quote, Medicare Plus Card, Gerber Life Insurance, National Magazine Exchange, NationalTaxRelief, Global DS Group, LLC, Hear Better for Life
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>--%>

  </fieldset>
                              </div>
      </div>
  </div>
 <table class="record_table">
    <tr>
        <td id="consent"><input type="checkbox" name="checkbox_2" id="opt_in" value="true" /></td>
        <td id="consentInfo">I Confirm that all of my information is accurate and consent to be contacted as provided above</td>
    </tr>
</table> 
           
      <input type="submit" id="btnSubmit" name="Submit" value="Submit" class="submit submit-btn" />
</form>



</div>
 
</div>
    <script type="text/javascript">
  $(function(){
    

    
      $('.signupForm').validate(
      {
      rules:{
         first_name:
                  {
                     required: true,
                     fNameRegex: true
                    },
             phone:
                {
                     required: true,
                     phoneRegex: true
          },
          checkbox_1:
          {
              required: true,
          },

        
        

            },
  messages:{
         first_name:
                  { 
                      required: "First Name is required.",
                      fNameRegex: "Please Enter a Valid First Name"
                    },
          phone:
                    {
                      required: "Phone is required.",
                      phoneRegex: "Please Enter a Valid Phone Number"
                     },
                    checkbox_1:
                      {
                          required: "Please agree to the Terms and Privacy to continue",
                      },
              },
              submitHandler: function (form) {
                    //check if phone number field exist
                    if($('#txt_phone').length > 0)
                    {
                      //unmask phone number if it exist before submit
                      $('#txt_phone').unmask();
                    }
                 //disable submit button
                  $('input[type="submit"]').attr('disabled','disabled');
                  console.log($('#txt_phone').val());
                  form.submit();
              },
                      errorPlacement: function(error, element) 
        {
            if ( element.is(':checkbox') ) 
            {
                console.log(error);
                    error.appendTo(".checkbox_wrapper");
            }
            else 
            { // This is the default behavior 
                error.insertAfter(element);
            }
        },


      });

        //first_name 
            $.validator.addMethod("fNameRegex",function(value,element)
            {
              return /^[a-zA-Z.\\'\\-\\s]+$/.test(value);
            });
          //phone
            $.validator.addMethod("phoneRegex",function(value,element)
            {
                return /^\(\d{3}\)\s?\d{3}-\d{4}$/.test(value);
            });

  }); 
  
 
</script>
    	<!-- Footer -->	
    <footer class="footer">
                  <!-- Skip Button -->
        <div class="row" id="skipButton" class="">
            <div class="col-md-12">
                <a onclick="clickCounter()" href="/api/offers"><h4 class="skip" style="color: #C6C4C1;float: right;padding-right: 20px;">Skip &nbsp;<i style="color:#C6C4C1;" class="fa fa-arrow-circle-right"></i></h4></a>
            </div>
        </div>    
         <div class="container text-center">
           Copyright &copy;
    		<span id="copyright">
        		<script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
    		</span>
   			Future In Stars, LLC 
          <a href="/Terms.aspx" target="_blank">Terms and Conditions</a> | <a href="/Privacy.aspx" target="_blank">Privacy Policy</a> | <a href="/Unsub.aspx" target="_blank">Unsubscribe</a>
        </div>  
    </footer>

    
    </asp:Content>