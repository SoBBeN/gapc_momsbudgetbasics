﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Recipe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Recipe Categories", "/Recipes/");
        string categoryId = Convert.ToString(Page.RouteData.Values["cat_id"]);
        string categoryType = Convert.ToString(Page.RouteData.Values["categoryType"]);
        string category = Convert.ToString(Page.RouteData.Values["category"]);
        breadcrumbs1.AddLevel(category.Replace("_", " "), "/Recipes/" + categoryId + "/" + categoryType + "/" + category);

        int id;

        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out id))
        {
            if (int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
                id = Convert.ToInt32(Page.RouteData.Values["id"]);
            else
                id = -1;
        }

        SqlDataReader dr;
        StringBuilder sb;

        if (id > 0)
        {
            dr = DB.DbFunctions.GetRecipe(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["ImageFilename"]);
                        breadcrumbs1.AddLevel(Convert.ToString(dr["Title"]), null);
                        fbComments1.Href = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Recipe/" + categoryId + "/" + categoryType + "/" + category + "/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                        ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                        ((ITmgMasterPage)Master).PageURL = "Recipe/" + categoryId + "/" + categoryType + "/" + category + "/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                        ((ITmgMasterPage)Master).PageType = "article";
                        ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Description"]);


                        litTitle.Text = Functions.ConvertToString(dr["Title"]);
                        litIngredients.Text = Functions.ConvertToString(dr["Ingredients"]);
                        litMethod.Text = Functions.ConvertToString(dr["Method"]);
                        litDate.Text = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d, yyyy"));
                        imgMain.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["ImageFilename"]);

                        string author = null;
                        author = Functions.ConvertToString(dr["Author"]);
                        string authorLink = null;
                        string authorImg = null;
                        if (dr["AuthorImage"] != DBNull.Value)
                            authorImg = String.Format("<img src=\"{0}\" runat=\"server\" class=\"imgAuthor\" alt=\"{1}\" />", System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["AuthorImage"]), author);

                        if (dr["AuthorLink"] != DBNull.Value)
                        {
                            authorLink = String.Format("<a href=\"{0}\" target=\"_blank\">", dr["AuthorLink"]);
                            if (authorImg != null)
                                litAuthorImage.Text = authorLink + authorImg + "</a>";
                            litAuthor.Text = authorLink + author + "</a>";
                        }
                        else
                        {
                            if (authorImg != null)
                                litAuthorImage.Text = authorImg;
                            litAuthor.Text = author;
                        }

                        

                        sb = new StringBuilder();
                        if (dr["CookTime"] != DBNull.Value || dr["PrepTime"] != DBNull.Value || dr["TotalTime"] != DBNull.Value || dr["Portions"] != DBNull.Value)
                        {
                            sb.Append("<br /><div>");
                            if (dr["CookTime"] != DBNull.Value)
                                sb.AppendFormat("<span><img src=\"/images/rectime.png\"> Cook Time:</span> {0} &nbsp; &nbsp; ", FormatTime(dr["CookTime"]));
                            if (dr["PrepTime"] != DBNull.Value)
                                sb.AppendFormat("<span><img src=\"/images/rectime.png\"> Prep Time:</span> {0} &nbsp; &nbsp; ", FormatTime(dr["PrepTime"]));
                            if (dr["TotalTime"] != DBNull.Value)
                                sb.AppendFormat("<span><img src=\"/images/rectime.png\"> Total Time:</span> {0} &nbsp; &nbsp; ", FormatTime(dr["TotalTime"]));
                            if (dr["Portions"] != DBNull.Value)
                                sb.AppendFormat("<span><img src=\"/images/recnumber.png\"> Portions:</span> {0}", dr["Portions"]);
                            sb.Append("</div>");
                        }

                        if (dr["Description"] != DBNull.Value)
                            sb.AppendFormat("<br />{0}", dr["Description"]);

                        litOthers.Text = sb.ToString();
                    }
                }
                dr.Close();
                dr.Dispose();

                // Tags (Categories) list
                dr = DB.DbFunctions.GetRecipeCategories(id);

                if (dr != null)
                {
                    if (dr.HasRows)
                    {
                        int groupby = 10;
                        sb = new StringBuilder();
                        while (dr.Read())
                        {
                            if (sb.Length > 0)
                                sb.Append(", ");
                            sb.AppendFormat("\n<a href=\"/Recipes/{0}/{1}/{2}/\">{3}</a>", dr["ID"], Functions.StrToURL(dr["Category"]), Functions.StrToURL(dr["Description"]), dr["Description"]);
                        }
                        litTags.Text = sb.ToString();
                    }
                    dr.Close();
                    dr.Dispose();
                }

            }
        }

        dr = DB.DbFunctions.GetRecipeCategoriesRandom();
        sb = new StringBuilder();

        if (dr != null)
        {
            string mylink = string.Empty;

            if (dr.HasRows)
            {
                mylink = "/Recipes/";
                var i = 0;
                while (dr.Read())
                {
                    i++;
                    string url = "Recipes/" + dr["ID"] + "/" + Functions.StrToURL(dr["Category"]) + "/" + Functions.StrToURL(dr["Description"]);

                    string link = String.Format("<a href=\"/{0}\">", url);
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Convert.ToString(dr["ImageFilename"]);
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Description"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"category\">");
                    sb.AppendFormat("<div class=\"img\">{0}<img src=\"{1}\"></a></div>", link, imageurl);
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append("</div>");

                }
                litCategories.Text = sb.ToString();
            }

            dr.Close();
            dr.Dispose();
        }
    }

    protected string FormatTime(object obj)
    {
        string str = String.Empty;
        if (obj != DBNull.Value && obj != null)
        {
            int time;
            if (int.TryParse(Convert.ToString(obj), out time))
            {
                if (time < 60)
                    str = time.ToString() + " mins";
                else
                    str = (time / 60).ToString() + ":" + (time % 60).ToString("X2");
            }
        }
        return str;
    }
}