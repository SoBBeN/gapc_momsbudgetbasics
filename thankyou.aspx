﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="thankyou.aspx.cs" Inherits="thankyou" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #content {
            background-image:url(images/boxheaderwelcome.jpg);
            background-repeat:no-repeat;
        }
        #thankyou,#thankyou2 {
            width: 100%;
            font-family: Arial;
            font-weight: bold;
            font-size: 22px;
            line-height: 1.2em;
            color: #69757f;
            margin: 10px 0;
            text-align: center;
           
        }
        #thankyou span {
            color: #ffcc00;
             text-shadow: 1px 1px 1px black;

        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">


    <div id="thankyou">Thanks for registering for <span>Mom's Budget Basic!</span></div>

    </asp:Content>