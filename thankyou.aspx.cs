﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class thankyou : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var email = Request["email"];
        var fname = Request["first_name"];
        var lname = Request["last_name"];
        var zip = Request["zip"];
        var address = Request["address_1"];
        var dob_day = Request["dob_d"];
        var dob_month = Request["dob_m"];
        var dob_year = Request["dob_y"];
        var dob = Request["dob"];
        var phone = Request["phone"];
        var checkbox_1 = Request["checkbox_1"];
        var checkbox_2 = Request["checkbox_2"];
        phone = "1" + phone;
        if (checkbox_2 == "true")
        {
            //do api call
            //var url = "https://rockwing.cloud.sloocetech.net/slooce_apps/spi/rockwing/" + phone + "/FUTUREINSTARS/messages/start";
            //var xmlText = "<message id=\"25624\"><partnerpassword>ZaH1sGx0miZcdUxO2</partnerpassword></message>";


            //
            //postXMLData(url, xmlText);

        }
    }
    public string postXMLData(string destinationUrl, string requestXml)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
        byte[] bytes;
        bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
        request.ContentType = "text/xml; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();
        HttpWebResponse response;
        response = (HttpWebResponse)request.GetResponse();

        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
        {
            Stream responseStream = response.GetResponseStream();
            string responseStr = new StreamReader(responseStream).ReadToEnd();
            return responseStr;
        }
        return null;
    }
}