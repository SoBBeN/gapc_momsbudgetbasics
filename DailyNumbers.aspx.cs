﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

public partial class DailyNumbers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            SqlDataReader dr;
            if (Request.QueryString["lid"] != null && Request.QueryString["lid"] != "")
            {
                dr = DB.DbFunctions.GetLeadData(int.Parse(Request.QueryString["lid"].ToString()));

                if (dr != null)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //var forwardedFor = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            //string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
                            string IpAddress = Request.UserHostAddress;
                            //DB.DbFunctions.PostToAllInbox("732", dr["SessionID"].ToString(), dr["emailaddress"].ToString(), dr["firstname"].ToString(), IpAddress, dr["lastname"].ToString(), dr["homephoneno"].ToString(), dr["referid"].ToString());
                        }
                    }
                }

                dr.Close();
                dr.Dispose();
            }

            dr = DB.DbFunctions.GetContent(73);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                        litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                        /*if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                        {
                            if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                                litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                            else
                                litImage.Text = "<a>";

                            string imageURL = System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/ag_c2w4l_banner.gif" + Functions.ConvertToString(dr["ImageFilename"]);

                            if(Functions.isMobile())
                            {
                                imageURL = System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/ag_c2w4l_banner.gif";
                            }

                            litImage.Text += "<img src=\"" + imageURL + "\" id=\"imgMain\" class=\"img\" />";
                            litImage.Text += "</a>";
                        }
                        else
                            litImage.Visible = false;*/
                    }
                }

                dr.Close();
                dr.Dispose();
            }
        }
    }
}