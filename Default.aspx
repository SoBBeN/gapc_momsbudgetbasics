﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%--    <asp:Literal runat="server" ID="LitHeadDefault"></asp:Literal>--%>
            <link href="css/MF_news.css" rel="stylesheet" />
        <link href="css/MF_lifestyle.css" rel="stylesheet" />
        <link href="css/MF_jobs.css" rel="stylesheet" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
        <style>
        .homeSections .ad6 a img {
            margin:0 auto !important;
        }
    </style>
    <div id="content">
        <asp:Literal runat="server" ID="litLog"></asp:Literal>


        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
<%--        <h1 class="clear"><a href="/Lifestyle"><img src="/images/MBB_lifestyleGlyph.png" /><asp:Literal runat="server" ID="litLifestylesTitle">Latest In <span>LifeStyles</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="lifestyles">
            <asp:Literal runat="server" ID="litArticles"></asp:Literal>
        </div>--%>
        <br class="clear" />
        <h1><a href="/News"><img src="/images/MBB_newsGlyph.png" /><asp:Literal runat="server" ID="litTitleAstroTalk">Latest In <span>News</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litNews"></asp:Literal>
        </div>

        <div class="clear"></div>
                        <hr />
        <div class="homeSections">
            <div><a href="/Samples/"><img src="/images/MBB_getfeesamples.png" /></a></div>
            <div><a href="/Rebates/"><img src="/images/MBB_getRebate.png" /></a></div>
            <div><a href="/CouponInsanity/"><img src="/images/MBB_getCoupon.png" /></a></div>
            <div><a href="/DailyNumbers/"><img src="/images/MBB_DailyNumbers.png" /></a></div>

        </div>
        <%--<div class="ads_padding_middle"></div>--%>
    </div>
    <div class="clear"></div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="OutsideForm" Runat="Server">
</asp:Content>




