﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="RecipeCategory.aspx.cs" Inherits="RecipeCategory" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/MF_lifestyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div id="content">
        <h1><asp:Literal runat="server" ID="litcategory"></asp:Literal></h1>
        <hr class="blueline" />
        <div id="lifestyles">
            <asp:Literal runat="server" ID="litOtherRecipes"></asp:Literal>
        </div>
        <div id="poPrevNext">
            <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< previous</a></div>
            <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">next ></a></div>
            <div style="clear:both;"></div>
        </div>
    </div>
</asp:Content>
