﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Collections.Generic;
using System.Xml;
using NSS.ROS;
using System.Diagnostics;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;





/// <summary>
/// Summary description for Functions
/// </summary>
public class Functions
{
    public static bool IsRegistred()
    {
        return (HttpContext.Current.Request.Cookies.Get("HMG_Registred") != null);
    }
    public static string StrToURL(object obj)
    {
        return StrToURL(Convert.ToString(obj));
    }
    public static string Left(String value, int length)
    {
        if (value == null || value.Length < length || value.IndexOf(" ", length) == -1)
            return value;

        return value.Substring(0, value.IndexOf(" ", length)) + "...";
    }
    public static string StrToImgUrl(string ImgName, string Title) // added by Adam
    {
        string Category = Regex.Match(Title, @"^(\w+\b.*?){1}").ToString();

        var CategoryArray = new Dictionary<string, string>
        { 
           { "Products", "Product" },
           { "Photos", "images" },
           { "Today", "Todaysmom" }
          
        };

        foreach (KeyValuePair<string, string> item in CategoryArray)
        {

            if (Category == item.Key)
            {
                Category = item.Value;
            }
            
        }
        string ImgUrl = "images/" + Category + "/" + ImgName ;

        return ImgUrl;
    }
    public static string RemoveSpace(string Str) // added by Adam
    {

        return Str.Replace(' ', '_').Replace('\'', '_').Replace('\"', '_');
    }
    public static string StrToURL(string str)
    {
        return str.Replace("?", "").Replace("“", "").Replace("”", "").Trim().Replace(' ', '_').Replace('.', '_').Replace('…', '_').Replace(':', '_').Replace("&", "And").Replace("+", "And").Replace("/", "_And_").Replace("%", "_").Replace("\"", "");
    }

    public static string RemoveSpecialChars(string str)
    {
        return str.Replace("?", "").Replace("“", "").Replace('…', '_').Replace(':', '_').Replace("&", "And").Replace("#", "");
    }

    public static string RemoveHtml(string str)
    {
        str = str.Replace("&nbsp;", " ");
        str = Regex.Replace(str, "<.*?>", " ").Replace('\n', ' ').Replace('\r', ' ').Replace('<', ' ').Replace('>', ' ');
        RegexOptions options = RegexOptions.None;
        Regex regex = new Regex(@"[ ]{2,}", options);
        str = regex.Replace(str, @" ");
        return str;
    }

    public static string ShortenText(string str, short length)
    {
        if (str.Length > length)
        {
            str = str.Substring(0, length);
            str = str.Substring(0, str.LastIndexOf(" ")) + " ...";
        }
        return str;
    }

    public static string ConvertToString(object obj)
    {
        if (DBNull.Value == obj)
            return string.Empty;
        else if (null == obj)
            return string.Empty;
        else
            return Convert.ToString(obj);
    }

    //Function to get random number
    private static readonly Random getrandom = new Random();
    private static readonly object syncLock = new object();
    public static int GetRandomNumber(int min, int max)
    {
        lock (syncLock)
        { // synchronize
            return getrandom.Next(min, max);
        }
    }

    public static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    public static string GetSha256(string str)
    {
        byte[] result;
        using (System.Security.Cryptography.SHA256 shaM = new System.Security.Cryptography.SHA256Managed())
        {
            result = shaM.ComputeHash(Encoding.Default.GetBytes(str));
        }
        return BitConverter.ToString(result).Replace("-", String.Empty);
    }

    public static string GetNonExistingFilename(string path, string filename)
    {
        int i = 0;
        string newfilename = filename;
        while (File.Exists(Path.Combine(path, newfilename)))
        {
            int index = filename.LastIndexOf(".");
            if (index >= 0)
                newfilename = filename.Substring(0, index) + (++i).ToString() + filename.Substring(index);
            else
                newfilename = filename.Substring(0, index) + (++i).ToString();
        }
        return newfilename;
    }

    public static string DoPost(ref string postToURL, ref string values, string contentType, string soapActionHeader)
    {
        return DoPost(ref postToURL, ref values, contentType, soapActionHeader, 60000);
    }

    public static string DoPost(ref string postToURL, ref string values, string contentType, string soapActionHeader, int timeout)
    {
        //Post Method Logic
        HttpWebRequest sendRequest = default(HttpWebRequest);
        sendRequest = (HttpWebRequest)WebRequest.Create(postToURL);
        sendRequest.Method = "POST";

        if (contentType == null)
        {
            contentType = "application/x-www-form-urlencoded";
        }
        else if (contentType.Length == 0)
        {
            contentType = "application/x-www-form-urlencoded";
        }
        sendRequest.ContentType = contentType;

        if ((soapActionHeader != null))
        {
            if (soapActionHeader.Length > 0)
            {
                sendRequest.Headers.Add("SOAPAction", soapActionHeader);
            }
        }

        sendRequest.Timeout = timeout;

        //Define content length
        sendRequest.ContentLength = values.Length;

        //Send Request
        StreamWriter myWriter = null;
        try
        {
            myWriter = new StreamWriter(sendRequest.GetRequestStream());
            myWriter.Write(values);
            myWriter.Flush();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if ((myWriter != null))
            {
                myWriter.Close();
            }
        }

        //Receive response
        //Receive response
        StreamReader reader = null;
        string responseFromPartner = null;
        StringBuilder errorText = new StringBuilder();
        try
        {
            try
            {
                if ((sendRequest.GetResponse().Headers != null))
                {
                    errorText.Append("Headers:" + Environment.NewLine);
                    foreach (string headerName in sendRequest.GetResponse().Headers)
                    {
                        errorText.Append(headerName + ":" + sendRequest.GetResponse().Headers.Get(headerName) + Environment.NewLine);
                    }
                    errorText.Append(Environment.NewLine);
                }
                errorText.AppendLine("ContentLength:" + sendRequest.GetResponse().ContentLength);
                errorText.AppendLine("ContentType:" + sendRequest.GetResponse().ContentType);

            }
            catch
            {
            }
            reader = new StreamReader(sendRequest.GetResponse().GetResponseStream());
            responseFromPartner = reader.ReadToEnd();
        }
        catch (WebException webex)
        {
            throw new WebException(webex.Message + Environment.NewLine + errorText.ToString(), webex.InnerException, webex.Status, webex.Response);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if ((reader != null))
            {
                reader.Close();
            }
        }

        return responseFromPartner;
    }

    public static string DoGet(ref string postToURL)
    {
        return DoGet(ref postToURL, 60000);
    }

    public static string DoGet(ref string postToURL, int timeout)
    {
        WebRequest sendRequest = default(WebRequest);
        sendRequest = WebRequest.Create(postToURL);
        sendRequest.Timeout = timeout;

        StreamReader reader = null;
        WebResponse response = null;
        string responseFromPartner = null;
        try
        {
            response = sendRequest.GetResponse();
            reader = new StreamReader(response.GetResponseStream());
            responseFromPartner = reader.ReadToEnd();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if ((reader != null))
            {
                reader.Close();
            }
            if ((response != null))
            {
                response.Close();
            }
        }

        return responseFromPartner;
    }

    public static bool IsIPValid(string userIPAddress)
    {
        if (userIPAddress == null)
            return false;
        else
        {
            userIPAddress = userIPAddress.Trim();

            if (userIPAddress.Length < 7)
                return false;
            else if (!Regex.IsMatch(userIPAddress, "^\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b$"))
            {
                return false;
            }
            else
            {
                try
                {
                    System.Net.IPAddress ipAddr = System.Net.IPAddress.Parse(userIPAddress);
                    userIPAddress = ipAddr.ToString();
                    int lev1 = default(int);
                    int lev2 = default(int);
                    int lev3 = default(int);
                    int lev4 = default(int);
                    lev2 = userIPAddress.IndexOf(".");
                    lev3 = userIPAddress.IndexOf(".", lev2 + 1);
                    lev4 = userIPAddress.IndexOf(".", lev3 + 1);
                    lev1 = Convert.ToInt32(userIPAddress.Substring(0, lev2));
                    lev2 = Convert.ToInt32(userIPAddress.Substring(lev2 + 1, lev3 - (lev2 + 1)));
                    lev3 = Convert.ToInt32(userIPAddress.Substring(lev3 + 1, lev4 - (lev3 + 1)));
                    lev4 = Convert.ToInt32(userIPAddress.Substring(lev4 + 1));
                    // Private Network (10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16)
                    if ((lev1 == 10) | (lev1 == 172 & lev2 >= 16 & lev2 < 32) | (lev1 == 192 & lev2 == 168))
                    {
                        return false;
                    }
                    // Link-local (169.254.0.0/16)
                    if ((lev1 == 169 & lev2 == 254))
                    {
                        return false;
                    }
                    // Loop Back (127.0.0.0/8)
                    if ((lev1 == 127))
                    {
                        return false;
                    }
                    // Current network (0.0.0.0/8)
                    if ((lev1 == 0))
                    {
                        return false;
                    }
                    // Broadcast (255.255.255.255/8)
                    if ((lev1 == 255 & lev2 == 255 & lev3 == 255 & lev4 == 255))
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }
        return true;
    }

    public static string GetDateSuffix(DateTime date)
    {
        // Get day...
        int day = date.Day;

        // Get day modulo...
        int dayModulo = day % 10;

        // Convert day to string...
        string suffix;

        // Combine day with correct suffix...
        suffix = (day == 11 || day == 12 || day == 13) ? "th" :
            (dayModulo == 1) ? "st" :
            (dayModulo == 2) ? "nd" :
            (dayModulo == 3) ? "rd" :
            "th";

        // Return result...
        return suffix;
    }

    public static bool isMobile()
    {
        string u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
        Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
        {
            return true;
        }

        return false;
    }
    /*Added Code For Job Page*/
    public static string GetReferID()
    {
        if (HttpContext.Current.Session["RefererID"] != null && Convert.ToString(HttpContext.Current.Session["RefererID"]).Length > 0)
            return Convert.ToString(HttpContext.Current.Session["RefererID"]);
        else if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["referID"]))
        {
            HttpContext.Current.Session["RefererID"] = HttpContext.Current.Request.QueryString["referID"];
            return HttpContext.Current.Request.QueryString["referID"];
        }
        else
            return String.Empty;
    }

    public static XmlDocument GetLocationInfoByIPAddress(string IPAddress)
    {
        try
        {
            string url = "http://freegeoip.net/xml/" + IPAddress;
            string xml;

            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            System.Net.WebResponse response = request.GetResponse();

            using (System.IO.Stream responseStream = response.GetResponseStream())
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, Encoding.UTF8);
                xml = reader.ReadToEnd();
            }

            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.LoadXml(xml);
            return xmlDoc;
        }
        catch (WebException e)
        { }
        catch (Exception ex)
        { }

        return new System.Xml.XmlDocument();
    }

    public static void PullGAPCDirectSaveIntoLead(bool overrideLead = false, string customTimeoutField = null)
    {
        Hashtable lead = FlowDirector.CurrentLead;

        if (lead["emailaddress"] != null && !String.IsNullOrEmpty(lead["emailaddress"].ToString()))
        {
            string emailAddress = lead["emailaddress"].ToString();

            Stopwatch requestTime = new Stopwatch();
            requestTime.Start();
            WebRequest request = WebRequest.Create("http://www.greatamericanphotocontest.com/directsave/ping.aspx?e=" + emailAddress);
            request.Headers.Add("auth_token", "ngy3p230hlg0wt=a.5230g809");
            request.Method = "GET";

            int requestTimeout = 200;
            if (customTimeoutField != null && lead[customTimeoutField] != null)
            {
                if (!int.TryParse(lead[customTimeoutField].ToString(), out requestTimeout))
                    requestTimeout = 200;
            }

            request.Timeout = requestTimeout;

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                requestTime.Stop();
                lead["custom11"] = requestTime.ElapsedMilliseconds.ToString();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
                    {
                        StringBuilder appliedDirectSaveFields = new StringBuilder();
                        StringBuilder existingC4RFields = new StringBuilder();
                        XmlDocument responseXml = new XmlDocument();
                        responseXml.LoadXml(responseReader.ReadToEnd());
                        XmlNode rootNode = responseXml.DocumentElement;
                        XmlNode resultNode = rootNode.SelectSingleNode("result");
                        if (resultNode != null && resultNode.InnerText.ToLower() == "success")
                        {
                            XmlNode recordNode = rootNode.SelectSingleNode("record");

                            if (recordNode != null)
                            {
                                string[] leadFields = new string[] {
                                "firstname|firstname",
                                "lastname|lastname",
                                "homephoneno|phone",
                                "address|address/address1",
                                "city|address/city",
                                "state|address/state",
                                "postalcode|address/zip",
                                "dateofbirth|birthdate"
                            };

                                foreach (string leadField in leadFields)
                                {
                                    string[] leadFieldSplit = leadField.Split(new char[] { '|' });
                                    string leadFieldValue = leadFieldSplit[0];
                                    string directSaveValue = leadFieldSplit[1];

                                    XmlNode directSaveNode = recordNode.SelectSingleNode(directSaveValue);

                                    bool emptyLeadField = lead[leadFieldValue] == null || String.IsNullOrEmpty(lead[leadFieldValue].ToString());
                                    bool emptyDirectSaveNode = directSaveNode == null || String.IsNullOrEmpty(directSaveNode.InnerText);

                                    if (!emptyLeadField)
                                    {
                                        existingC4RFields.Append(leadFieldValue).Append("|");
                                    }

                                    if ((emptyLeadField || overrideLead) && !emptyDirectSaveNode)
                                    {
                                        appliedDirectSaveFields.Append(leadFieldValue).Append("|");
                                        lead[leadFieldValue] = directSaveNode.InnerText;
                                    }
                                }
                            }
                            lead["custom7"] = existingC4RFields.ToString();
                            lead["custom8"] = appliedDirectSaveFields.ToString();
                        }
                        else
                        {
                            lead["custom8"] = "N/A";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("PullGAPCDirectSaveIntoLead", ex, "URL: http://www.greatamericanphotocontest.com/directsave/ping.aspx?e=" + emailAddress);
                lead["custom11"] = "Timed out";
            }
        }
    }
    public static string GetAffiliateName(Boolean mapAffName = false)
    {
        string referid = String.Empty;
        string affname = String.Empty;
        try
        {
            referid = GetReferID();

            int index = referid.IndexOf("_");
            if (index > 1)
            {
                affname = referid.Substring(0, index);
            }
            else if (!String.IsNullOrEmpty(referid))
            {
                if (referid.StartsWith("gg"))
                    affname = "Google";
                else
                    affname = referid;
            }

            if (mapAffName)
            {
                try
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Flow_ReferMask ");
                    sb.Append("'").Append(sql.q(affname)).Append("'");

                    affname = sql.GetScalar(sb.ToString()).ToString();
                }
                catch (Exception ex)
                {
                    ErrorHandling.SendException("GetAffiliateName ReferMask", ex);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandling.SendException("GetCIPîxel Referid", ex);
        }

        return affname;
    }

    public static RegDataCompletionLevel CheckRegDataCompletion(Hashtable lead, string[] fieldsToVerifyIn = null)
    {
        string[] fieldsToVerify = fieldsToVerifyIn == null ? new string[] { "firstname", "lastname", "address", "homephoneno", "dateofbirth", "postalcode" } : fieldsToVerifyIn;
        int fieldsPassed = 0;
        foreach (string fieldToVerify in fieldsToVerify)
        {
            if (lead[fieldToVerify] != null && !String.IsNullOrEmpty(lead[fieldToVerify].ToString()))
            {
                if (fieldToVerify == "dateofbirth")
                {
                    DateTime dobVerify = DateTime.MinValue;
                    if (DateTime.TryParse(lead[fieldToVerify].ToString(), out dobVerify))
                        fieldsPassed++;
                }
                else
                    fieldsPassed++;
            }
        }

        if (fieldsPassed == fieldsToVerify.Length)
            return RegDataCompletionLevel.Full;
        else if (fieldsPassed > 0)
            return RegDataCompletionLevel.Partial;
        else
            return RegDataCompletionLevel.None;


    }

    public enum RegDataCompletionLevel
    {
        None = 0,
        Partial = 1,
        Full = 2
    }
    /*Added Code For Job Page End*/
}