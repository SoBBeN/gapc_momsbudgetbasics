﻿using DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for LifeStyle
/// </summary>
public class LifeStyle
{
	private int WebSiteID;
	private StringBuilder sb;
	private SqlServer objSqlServer;

	public LifeStyle(int WebSiteID)
	{
		this.WebSiteID = WebSiteID;
	}
	public SqlDataReader GetLifestyleList()
	{
	    objSqlServer = new SqlServer();
		StringBuilder query = new StringBuilder("[Lifestyle_GetList] ");
		query.Append("Null").Append(",");
		query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
		query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

		return objSqlServer.GetDataReader(query.ToString());
	}

	public string GetLifestyleThumbNails()
	{
		sb = new StringBuilder();
		var DateBaseReader = GetLifestyleList();

		if (DateBaseReader.HasRows)
		{
			DateBaseReader.NextResult(); //used because we got 2 different tables in sql one for categories and another for lifestyle articles

			while (DateBaseReader.Read())
			{

				string link = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Convert.ToString(DateBaseReader["Thumbnail"]);
				string imageSrc = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Functions.ConvertToString(DateBaseReader["Thumbnail"]);
				string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(DateBaseReader["DescriptionTop"]) + " " + Convert.ToString(DateBaseReader["Description"])), 150).Replace("\"", " ").Trim();
				string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(DateBaseReader["Title"])), 300).Replace("\"", " ").Trim();

				sb.AppendFormat("<div id='lifestyles'><div class='lifestyle'><div class='img'><a href='{0}'><img src='{1}' /></a></div><div class='description'><div class='title'><a href='{0}'>{2}</a></div>{3}</div></div></div>", link, imageSrc, title, description);
			}
			DateBaseReader.Close();
			DateBaseReader.Dispose();
		}
		return sb.ToString();
	}

	public string GetLifestyleThumbNails(int amount)
	{
		int count = 0;
		sb = new StringBuilder();
		var DateBaseReader = GetLifestyleList();

		if (DateBaseReader.HasRows)
		{
			DateBaseReader.NextResult(); //used because we got 2 different tables in sql one for categories and another for lifestyle articles

			while (DateBaseReader.Read() && count++ < amount)
			{
				

				string link = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Convert.ToString(DateBaseReader["Thumbnail"]);
				string imageSrc = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Functions.ConvertToString(DateBaseReader["Thumbnail"]);
				string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(DateBaseReader["DescriptionTop"]) + " " + Convert.ToString(DateBaseReader["Description"])), 150).Replace("\"", " ").Trim();
				string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(DateBaseReader["Title"])), 300).Replace("\"", " ").Trim();

				sb.AppendFormat("<div id='lifestyles'><div class='lifestyle'><div class='img'><a href='{0}'><img src='{1}' /></a></div><div class='description'><div class='title'><a href='{0}'>{2}</a></div>{3}</div></div></div>", link, imageSrc, title, description);
				if (count < amount)
				{
					sb.Append("<div style='clear: both;'><br /><hr class='blueline' /></div>");
				}

			}
			DateBaseReader.Close();
			DateBaseReader.Dispose();
		}
		return sb.ToString();
	}
}