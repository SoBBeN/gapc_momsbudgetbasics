﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LifestyleCategories : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;
    //WebSiteInfo website;
    //LifeStyle lifestyle;

    protected void Page_Load(object sender, EventArgs e)
    {
        //website = new WebSiteInfo(Request.Url.Host);
        //lifestyle = new LifeStyle(website.GetWebSiteID());

        SqlDataReader dr;

        int id;
        string mycategory = string.Empty;
        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = int.MinValue;
        }
        else
            mycategory = Convert.ToString(Page.RouteData.Values["title"]);

        dr = DB.DbFunctions.GetLifestyle(108); //For Threading only

        if (dr != null)
        {
            string mylink = string.Empty;
            if (dr.HasRows)
            {
                StringBuilder sb = new StringBuilder();
                bool first = true;
                while (dr.Read())
                {
                    if (first)
                        first = false;

                    //mylink = "/Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(mycategory) + "/";
                    mylink = "/Lifestyle";

                    if (Convert.ToBoolean(dr["Selected"]))
                    {

                        mycategory = Convert.ToString(dr["Description"]);
                        //Changed title name to Career Talk requested by lloyd
                        //mycategory = "Sweeps Talk";


                        breadcrumbs1.AddLevel(mycategory, mylink);
                        ((ITmgMasterPage)Master).PageTitle = mycategory;
                        ((ITmgMasterPage)Master).PageURL = mylink;
                        ((ITmgMasterPage)Master).PageType = "article";
                        //((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);
                        litcategory.Text = mycategory;
                    }
                }
            }

            dr.NextResult();

            if (dr.HasRows)
            {
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = "/Lifestyle?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = "/Lifestyle?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read()) ;

                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string url = "Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(mycategory) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Convert.ToString(dr["Thumbnail"]);
                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["Description"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append(link).AppendFormat("<div class=\"img\"><img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"], Functions.ConvertToString(dr["Thumbnail"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(description);
                    sb.Append("</div>");

                    sb.Append("</div><div style=\"clear:both;\"></div><br /><hr class=\"blueline\" />");
  
     
                    //if (i % 3 == 0)
                    //{
                    //    if (Request.Browser.IsMobileDevice)
                    //    {
                    //        sb.Append("<div class=\"ad\">");
                    //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                    //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    //    }
                    //}
                }

                //if (i < 3)
                //{
                //    if (Request.Browser.IsMobileDevice)
                //    {
                //        sb.Append("<div class=\"ad\">");
                //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                //    }
                //}

                litArticles.Text = sb.ToString();
                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }

            dr.Close();
            dr.Dispose();
        }
    }
}