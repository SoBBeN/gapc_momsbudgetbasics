﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPageAnswer : ITmgMasterPage
{

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            UserSession.InsertImpression();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("\n    <title>");
            if (pagetitle != System.Configuration.ConfigurationManager.AppSettings["title"])
                sb.Append(System.Configuration.ConfigurationManager.AppSettings["title"] + " - ");
            sb.Append(pagetitle);
            sb.AppendLine("</title>");
            sb.Append("    <link rel=\"image_src\" href=\"").Append(HttpUtility.UrlPathEncode(pagelogo)).AppendLine("\" />");

            if (showOGtags)
            {
                sb.Append("    <meta property=\"og:title\" content=\"").Append(pagetitle).AppendLine("\" />");
                sb.Append("    <meta property=\"og:url\" content=\"").Append(pageurl).AppendLine("\" />");
                sb.Append("    <meta property=\"og:image\" content=\"").Append(HttpUtility.UrlPathEncode(pagelogo)).AppendLine("\" />\n");
                sb.Append("    <meta property=\"og:type\" content=\"").Append(pagetype).AppendLine("\" />\n");
                if (pagedescription.Length > 0)
                {
                    sb.Append("    <meta property=\"og:description\" content=\"").Append(pagedescription).AppendLine("\" />\n");
                    sb.Append("    <meta name=\"description\" content=\"").Append(pagedescription).AppendLine("\" />\n");
                }
                foreach (string img in pageimg)
                {
                    sb.Append("    <meta property=\"og:image\" content=\"").Append(HttpUtility.UrlPathEncode(img)).AppendLine("\" />\n");
                }
            }
            lithead.Text = sb.ToString();


            SqlDataReader dr = DB.DbFunctions.GetContentCategories();

            if (dr != null)
            {
                string mylink = string.Empty;
                if (dr.HasRows)
                {
                    sb = new StringBuilder();
                    bool first = true;
                    while (dr.Read())
                    {
                        if (first)
                            first = false;
                        else
                            sb.Append("<hr />");

                        string link = "/Content/" + Functions.StrToURL(Convert.ToString(dr["ID"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/";

                        if(int.Parse(dr["ID"].ToString()) == 50)
                        {
                            link = "/Rebates";
                        }

                        if (int.Parse(dr["ID"].ToString()) == 56)
                        {
                            link = "/News/";
                        }

                        sb.AppendFormat("<a href=\"{0}\">{1}</a>", link, Convert.ToString(dr["Category"]));
                    }

                    if (Session["questions"] != null && Session["questions"] != String.Empty)
                    {
                        sb.Append("<hr />");
                        sb.Append("<a href=\"/Answer.aspx\">Question Results</a>");
                    }

                    litCategories.Text = sb.ToString();
                }
            }

            dr.Close();
            dr.Dispose();
        }
    }
}
